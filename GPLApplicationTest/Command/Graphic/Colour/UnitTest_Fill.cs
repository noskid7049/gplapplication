﻿using System;
using GPLApplication;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GPLApplicationTest
{
    /// <summary>
    /// Test for Fill Command
    /// This tests test the commands of the fill command
    /// </summary>
    [TestClass]
    public class UnitTest_Fill
    {
        /// <summary>
        /// Tests the SetParams function of the fill command
        /// </summary>
        /// <param name="command">Command of type fill with params</param>
        /// <param name="fill">Bool to check if the command turns the fill on or off</param>
        [DataTestMethod]
        [DataRow("fill on", true)]
        [DataRow("fill off", false)]
        public void Test_SetParams(string command, bool fill)
        {
            OutputCanvas testCanvas = new OutputCanvas();
            CommandWrapper commandWrapper = CommandFactory.getCommand(command, testCanvas);
            string[] paramsArr = Helper.SplitString(Helper.SplitString(command, " ")[1], ",");
            Fill obj = (Fill)commandWrapper.ActualCommand;
            obj.SetParams(paramsArr);
            Assert.AreEqual(obj.ShapeFill, fill);
        }
    }
}
