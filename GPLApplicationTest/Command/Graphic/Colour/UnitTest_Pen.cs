﻿using System;
using GPLApplication;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GPLApplicationTest
{
    /// <summary>
    /// Test for Pen Command
    /// </summary>
    [TestClass]
    public class UnitTest_Pen
    {
        /// <summary>
        /// Testst the SetParams function of the Pen command
        /// </summary>
        /// <param name="command">Required command string</param>
        /// <param name="color">Expected output (color) in string</param>
        [DataTestMethod]
        [DataRow("pen black", "Color [Black]")]
        [DataRow("pen red", "Color [Red]")]
        public void Test_SetParams(string command, string color)
        {
            OutputCanvas testCanvas = new OutputCanvas();
            CommandWrapper commandWrapper = CommandFactory.getCommand(command, testCanvas);
            string[] paramsArr = Helper.SplitString(Helper.SplitString(command, " ")[1], ",");
            PenC obj = (PenC)commandWrapper.ActualCommand;
            obj.SetParams(paramsArr);
            Console.WriteLine(obj.PenColor.ToString());
            Assert.AreEqual(obj.PenColor.ToString(), color);
        }
    }
}
