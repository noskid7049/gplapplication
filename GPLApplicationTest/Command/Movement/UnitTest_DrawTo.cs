﻿using System;
using System.Drawing;
using GPLApplication;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GPLApplicationTest
{
    /// <summary>
    /// Tests for the DrawTo command
    /// </summary>
    [TestClass]
    public class UnitTest_DrawTo
    {
        /// <summary>
        /// Tests the SetParams for the DrawTo command
        /// </summary>
        /// <param name="command">Command string to check</param>
        /// <param name="X">Expected X output</param>
        /// <param name="Y">Expected Y output</param>
        [DataTestMethod]
        [DataRow("drawto 100,100", 100, 100)]
        [DataRow("drawto 100,200", 100, 200)]
        [DataRow("drawto 200,200", 200, 200)]
        public void Test_SetParams(string command, int X, int Y)
        {
            OutputCanvas testCanvas = new OutputCanvas();
            CommandWrapper commandWrapper = CommandFactory.getCommand(command, testCanvas);
            string[] paramsArr = Helper.SplitString(Helper.SplitString(command, " ")[1], ",");
            DrawTo obj = (DrawTo)commandWrapper.ActualCommand;
            Console.WriteLine(obj == null);
            obj.SetParams(paramsArr);
            Assert.AreEqual(X, obj.EndPoint.X);
            Assert.AreEqual(Y, obj.EndPoint.Y);
        }
    }
}
