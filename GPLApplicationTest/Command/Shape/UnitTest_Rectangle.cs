﻿using System;
using System.Drawing;
using GPLApplication;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GPLApplicationTest
{
    /// <summary>
    /// Tests for the rectangle command
    /// </summary>
    [TestClass]
    public class UnitTest_Rectangle
    {
        /// <summary>
        /// Tests the SetParams method of the rectangle command
        /// </summary>
        /// <param name="command">Command to check</param>
        /// <param name="expWidth">Expected width</param>
        /// <param name="expHeight">Expected height</param>
        [DataTestMethod]
        [DataRow("rect 100,100", 100f, 100f)]
        [DataRow("rect 100,120", 100f, 120f)]
        [DataRow("rect 200,200", 200f, 200f)]
        public void Test_SetParams(string command, float expWidth, float expHeight)
        {
            OutputCanvas testCanvas = new OutputCanvas();
            CommandWrapper commandWrapper = CommandFactory.getCommand(command, testCanvas);
            string[] paramsArr = Helper.SplitString(Helper.SplitString(command, " ")[1], ",");
            Console.WriteLine(paramsArr[0]);
            RectangleC obj = (RectangleC)commandWrapper.ActualCommand;
            obj.SetParams(paramsArr);
            Assert.AreEqual(expWidth, obj.Width);
            Assert.AreEqual(expHeight, obj.Height);
        }
    }
}
