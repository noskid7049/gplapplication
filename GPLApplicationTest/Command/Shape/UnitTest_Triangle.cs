﻿using System;
using System.Drawing;
using GPLApplication;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GPLApplicationTest
{
    /// <summary>
    /// Tests for the triangle command
    /// </summary>
    [TestClass]
    public class UnitTest_Triangle
    {
        /// <summary>
        /// Test the SetParams method of the triangle command
        /// </summary>
        /// <param name="command">Command to check</param>
        /// <param name="expWidth">Expected width set</param>
        /// <param name="expHeight">Expected height set</param>
        [DataTestMethod]
        [DataRow("triangle 100,100", 100f, 100f)]
        [DataRow("triangle 100,120", 100f, 120f)]
        [DataRow("triangle 200,200", 200f, 200f)]
        public void Test_SetParams(string command, float expWidth, float expHeight)
        {
            OutputCanvas testCanvas = new OutputCanvas();
            CommandWrapper commandWrapper = CommandFactory.getCommand(command, testCanvas);
            string[] paramsArr = Helper.SplitString(Helper.SplitString(command, " ")[1], ",");
            Console.WriteLine(paramsArr[0]);
            Triangle obj = (Triangle)commandWrapper.ActualCommand;
            obj.SetParams(paramsArr);
            Assert.AreEqual(expWidth, obj.Width);
            Assert.AreEqual(expHeight, obj.Height);
        }
    }
}
