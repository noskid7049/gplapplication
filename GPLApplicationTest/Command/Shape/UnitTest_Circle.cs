﻿using System;
using System.Drawing;
using GPLApplication;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GPLApplicationTest
{
    /// <summary>
    /// Tests for the Circle command
    /// </summary>
    [TestClass]
    public class UnitTest_Circle
    {
        /// <summary>
        /// Tests the SetParams method of the circle command
        /// </summary>
        /// <param name="command">Command to check</param>
        /// <param name="expRadius">Expected radius</param>
        [DataTestMethod]
        [DataRow("circle 100", 100)]
        [DataRow("circle 100", 100)]
        [DataRow("circle 200", 200)]
        public void Test_SetParams(string command, float expRadius)
        {
            OutputCanvas testCanvas = new OutputCanvas();
            CommandWrapper commandWrapper = CommandFactory.getCommand(command, testCanvas);
            string[] paramsArr = Helper.SplitString(Helper.SplitString(command, " ")[1], ",");
            Circle obj = (Circle)commandWrapper.ActualCommand;
            obj.SetParams(paramsArr);
            Assert.AreEqual(expRadius, obj.Radius);
        }
    }
}
