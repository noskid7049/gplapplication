﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GPLApplication;

namespace GPLApplicationTest
{
    /// <summary>
    /// Tests for the DynamicFactory class
    /// </summary>
    [TestClass]
    public class UnitTest_DynamicFactory
    {
        /// <summary>
        /// Tests for the Create method of the DynamicFactory class
        /// </summary>
        /// <param name="commandName">Command name to </param>
        /// <param name="output">Expected output</param>
        [DataTestMethod]
        [DataRow("MoveTo", "moveto")]
        [DataRow("Circle", "circle")]
        [DataRow("RectangleC", "rect")]
        public void Test_Create(string commandName, string output)
        {
            ICommand command = DynamicFactory<ICommand>.Create(commandName);
            Assert.AreEqual(output, command.CommandName());
        }
    }
}
