﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GPLApplication;

namespace GPLApplicationTest
{
    /// <summary>
    /// Test for the CommandParser class
    /// </summary>
    [TestClass]
    public class UnitTest_CommandParser
    {
        /// <summary>
        /// Tests the ParseLine function
        /// </summary>
        /// <param name="line">Line to parse</param>
        /// <param name="command">Expected command output</param>
        /// <param name="argsArr">Expected arguments in string</param>
        [DataTestMethod]
        [DataRow("moveto 100,100", "moveto", new string[] { "100", "100" })]
        [DataRow("circle 100", "circle", new string[] { "100" })]
        public void Test_ParseLine(string line, string command, string[] argsArr)
        {
            CommandWrapper test = CommandParser.ParseLine(line);
            Assert.AreEqual(command, test.CommandName);
            CollectionAssert.AreEqual(argsArr, test.CommandParamsArr);
        }
        /// <summary>
        /// Tests the multiline checker of the command parser
        /// </summary>
        /// <param name="line">Line to check</param>
        /// <param name="isMultiline">Expected true/false for multiline</param>
        [DataTestMethod]
        [DataRow("if test==10{", true)]
        [DataRow("if test==10 ", false)]
        [DataRow("while count < 0{  ", true)]
        public void Test_MultiLineCommand(string line, bool isMultiline)
        {
            bool correct = false;
            if (line.Contains("{"))
            {
                correct = true;
            }
            Assert.AreEqual(isMultiline, correct);
        }
        /// <summary>
        /// Tests the isAssigmentCommand of the command parser class
        /// </summary>
        /// <param name="line">Line to check</param>
        /// <param name="isMultiline">Expected output</param>
        [DataTestMethod]
        [DataRow("test=10", true)]
        [DataRow("test==10 ", false)]
        [DataRow("test>10  ", false)]
        public void Test_IsAssignmentCommand(string line, bool isMultiline)
        {
            bool correct = false;
            string assignmentOperator = "=";
            string[] conditionalOperators = new string[] { "==", "<=", ">=", "&&", "||","<",">" };
            if (line.Contains(assignmentOperator)) correct = true;
            foreach (string op in conditionalOperators)
            {
                if (line.Contains(op))
                {
                    correct = false;
                    break;
                }
            }
            Assert.AreEqual(isMultiline, correct);
        }
        /// <summary>
        /// Tests the IsConditionalCommand of the command parser class
        /// </summary>
        /// <param name="line">Line to check</param>
        /// <param name="isMultiline">Expected output</param>
        [DataTestMethod]
        [DataRow("test>=10", true)]
        [DataRow("test==10 ", true)]
        [DataRow("test=10  ", false)]
        public void Test_IsConditionalCommand(string line, bool isMultiline)
        {
            bool correct = false;
            string[] conditionalOperators = new string[] { "==", "<=", ">=", "&&", "||", "<", ">" };
            foreach (string op in conditionalOperators)
            {
                if (line.Contains(op))
                {
                    correct = true;
                    break;
                }
            }
            Assert.AreEqual(isMultiline, correct);
        }

    }
}
