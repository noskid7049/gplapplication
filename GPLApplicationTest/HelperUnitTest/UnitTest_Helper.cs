﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GPLApplication;
using System.Collections.Generic;

namespace GPLApplicationTest
{
    /// <summary>
    /// Tests for the Helper class
    /// </summary>
    [TestClass]
    public class UnitTest_Helper
    {
        /// <summary>
        /// Tests for the SplitString method of the Helper class
        /// </summary>
        /// <param name="line">Line to split</param>
        /// <param name="character">Character to use to split the line</param>
        /// <param name="pieces">Expected pieces after split</param>
        [DataTestMethod]
        [DataRow("moveto 100,100", " ", new string[] { "moveto", "100,100" })]
        [DataRow("'100,100',100", ",", new string[] { "100,100", "100" })]
        [DataRow("moveto 100,100\ncircle 100", "\n", new string[] { "moveto 100,100", "circle 100" })]
        public void Test_SplitString(string line, string character, string[] pieces)
        {
            var split = Helper.SplitString(line, character);
            CollectionAssert.AreEqual(pieces, split);
        }
        /// <summary>
        /// Tests for the SplitStringBrackets method of the Helper class
        /// </summary>
        /// <param name="line">Line to split</param>
        /// <param name="character">Character to use for the splitting</param>
        /// <param name="pieces">String pieces after the line has been split</param>
        [DataTestMethod]
        [DataRow("(100,100),(150,200)", ",", new string[] { "(100,100)", "(150,200)" })]
        public void Test_SplitStringBrackets(string line, string character, string[] pieces)
        {
            var split = Helper.SplitStringBrackets(line, character);
            CollectionAssert.AreEqual(pieces, split);
        }
        /// <summary>
        /// Tests for the IsArithmeticOperation of the Helper class
        /// </summary>
        /// <param name="command">Command string to check</param>
        /// <param name="expectedOutput">Expected output</param>
        [DataTestMethod]
        [DataRow("var = 1", true)]
        [DataRow("var+=1", true)]
        [DataRow("var++", true)]
        [DataRow("var", false)]
        public void Test_IsArithmeticOperation(string command, bool expectedOutput)
        {
            bool output = Helper.IsArithmeticOperation(command);
            Assert.AreEqual(expectedOutput, output);
        }
        /// <summary>
        /// Tests fro the IsValidConditionOrLoop function of the Helper static class
        /// </summary>
        /// <param name="command">Command to check</param>
        /// <param name="statement">Statement to check against</param>
        /// <param name="expectedOutput">Expected output bool</param>
        [DataTestMethod]
        [DataRow("if(true)", "if", true)]
        [DataRow("while(true)", "while", true)]
        public void Test_IsValidConditionOrLoop(string command, string statement, bool expectedOutput)
        {
            bool output = Helper.IsValidConditionalOrLoop(command, statement);
            Assert.AreEqual(expectedOutput, output);
        }
        /// <summary>
        /// Tests for the IsValidMethodDefiner function of the Helper static class
        /// </summary>
        /// <param name="command">Command to check</param>
        /// <param name="expectedOutput">Expected output</param>
        [DataTestMethod]
        [DataRow("method test()", true)]
        [DataRow("method test(x,y)", true)]
        [DataRow("function test()", false)]
        public void Test_IsValidMethodDefiner(string command, bool expectedOutput)
        {
            bool output = Helper.IsValidMethodDefiner(command);
            Assert.AreEqual(expectedOutput, output);
        }
        /// <summary>
        /// Tests for the ParseLineType method of the Helper class
        /// </summary>
        /// <param name="command">Command to parse</param>
        /// <param name="eOutput">Expected statement</param>
        /// <param name="eType">Expected type of the statement</param>
        /// <param name="ePosition">Expected position of the statement</param>
        [DataTestMethod]
        [DataRow("if(test == 0)","if","conditional","start")]
        [DataRow("end if", "if", "conditional", "end")]
        [DataRow("while(i<10)", "while", "loop", "start")]
        public void Test_ParseLineType(string command, string eOutput, string eType, string ePosition)
        {
            Dictionary<string, string> commandParse = Helper.ParseLineType(command);
            commandParse.TryGetValue("output", out string statement);
            commandParse.TryGetValue("type", out string type);
            commandParse.TryGetValue("position", out string position);

            Assert.AreEqual(eOutput, statement);
            Assert.AreEqual(eType, type);
            Assert.AreEqual(ePosition, position);
        }
    }
}
