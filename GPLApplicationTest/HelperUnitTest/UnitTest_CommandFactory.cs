﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GPLApplication;
namespace GPLApplicationTest
{
    /// <summary>
    /// Tests for CommandFactory class
    /// </summary>
    [TestClass]
    public class UnitTest_CommandFactory
    {
        /// <summary>
        /// Tests for the GetCommand method
        /// </summary>
        /// <param name="line">Line to parse</param>
        /// <param name="commandName">Expected command</param>
        /// <param name="paramArr">Expected params from the command</param>
        [DataTestMethod]
        [DataRow("moveto 100,100", "moveto", new string[] { "100", "100" })]
        [DataRow("rect 100,100", "rect", new string[] { "100", "100" })]
        [DataRow("circle 100", "circle", new string[] { "100" })]
        public void Test_getCommand(string line, string commandName, string[] paramArr)
        {

            OutputCanvas testCanvas = new OutputCanvas();
            CommandWrapper test = CommandFactory.getCommand(line, testCanvas);
            Assert.AreEqual(commandName, test.ActualCommand.CommandName());
            CollectionAssert.AreEqual(paramArr, test.CommandParamsArr);
        }
    }
}
