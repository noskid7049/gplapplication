﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// Static class to hold the regex patterns
    /// </summary>
    public static class RegexObject
    {
        // Regex for operators
        /// <summary>
        /// Assignment operator Regex
        /// </summary>
        public static string assignmentOp = @"=|\+=|\-=|\*=|\/=|\%=|<<=|>>=|&=|^=|\|=";
        /// <summary>
        /// Unary Operation Regex
        /// </summary>
        public static string unaryOp = @"\+\+|\-\-";
        /// <summary>
        /// Binary operation Regex
        /// </summary>
        public static string binaryOp = @"<<|>>|\+|-|\*|/|%|&&|\|\||&|\||\^|==|!=|>=|<=|=|<|>";
        /// <summary>
        /// Regex for the arithmetic operations
        /// </summary>
        public static Regex arithmeticOperation = new Regex(@"^\b(?<variable>\w+)(?<operator>\s*(?:" + assignmentOp + ")|(?:" + unaryOp + @")\s*)(?<value>(\w|\s|""|'|(?:" + binaryOp + ")|(?:" + unaryOp + @"))*)$");

        // Regex for method definer
        /// <summary>
        /// Method Definer regex
        /// </summary>
        public static Regex regexMethodDefiner = new Regex(@"^method\s+\b(?<methodName>\w+)\b\((?<paramString>((\s*\w+\s*,*)*(\s*\w+))*)\)$");
        /// <summary>
        /// Method call regex
        /// </summary>
        public static Regex regexMethodCall = new Regex(@"^\b(?<methodName>\w+)\b\((?<paramString>((\s*\w+\s*,*)*(\s*\w+))*)\)$");
        /// <summary>
        /// Function to generate conditional regex
        /// </summary>
        /// <param name="statement">Statement to generate the regex for</param>
        /// <returns>The required regex</returns>
        public static Regex GenerateConditionalRegex(string statement)
        {
            var regex = new Regex(@"(?<=" + statement + @")\s*\({1}(?<condition>.*)\){1}(?<command>.*)");
            return regex;
        }
        /// <summary>
        /// Function to generate the start of a statement
        /// </summary>
        /// <param name="statement">Statement to generate the start of </param>
        /// <returns>Regex for the start </returns>
        public static Regex GenerateStatementStart(string statement)
        {
            return new Regex(@"^" + statement + @"");
        }
        /// <summary>
        /// Function to generate the end of the statement
        /// </summary>
        /// <param name="statement">Statement to generate the end of</param>
        /// <returns>Regex for the end</returns>
        public static Regex GenerateStatementEnd(string statement)
        {
            return new Regex(@"end\s+" + statement + @"$");
        }
    }
}
