﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace GPLApplication
{
    /// <summary>
    /// Extension for the dictionary class to allow appending of one dictionary Key-Value pair to another dictionary
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Function to append 
        /// </summary>
        /// <typeparam name="K">Dictionary Key</typeparam>
        /// <typeparam name="V">Dictionary Value</typeparam>
        /// <param name="first">Dictionary to append to</param>
        /// <param name="second">Dictionary to append</param>
        public static void Append<K, V>(this Dictionary<K, V> first, Dictionary<K, V> second)
        {
            second.ToList()
                .ForEach(pair => first[pair.Key] = pair.Value);
        }
    }
    /// <summary>
    /// Static class to hold the default App Properties/ Values of the application
    /// </summary>
    public static class AppProperties
    {
        // Dictionaries of commands
        /// <summary>
        /// Color commands dictionary
        /// </summary>
        public static Dictionary<string, string> colorCommands = new Dictionary<string, string>()
        {
            { "pen", "PenC" },
            { "fill", "Fill" }
        };
        /// <summary>
        /// Movement commands dictionary
        /// </summary>
        public static Dictionary<string, string> movementCommands = new Dictionary<string, string>()
        {
            { "moveto", "MoveTo" },
            { "drawto", "DrawTo" }
        };
        /// <summary>
        /// Keyword commands dictionary
        /// </summary>
        public static Dictionary<string, string> keywordCommands = new Dictionary<string, string>()
        {
            { "reset", "Reset" },
            { "clear", "Clear" },
        };
        /// <summary>
        /// Shape commands dictionary
        /// </summary>
        public static Dictionary<string, string> shapeCommands = new Dictionary<string, string>()
        {
            { "rect", "RectangleC" },
            { "triangle", "Triangle" },
            { "circle", "Circle" },
            { "polygon", "Polygon" },
        };
        // Function to concat all the dictionaries of commands
        /// <summary>
        /// Function that concats all the commands dictionary
        /// </summary>
        /// <returns>Dictionary with all the required commands</returns>
        private static Dictionary<string, string> ConcatAllCommands()
        {
            Dictionary<string, string> tmp = new Dictionary<string, string>();
            tmp.Append(colorCommands);
            tmp.Append(movementCommands);
            tmp.Append(keywordCommands);
            tmp.Append(shapeCommands);
            return tmp;
        }
        /// <summary>
        /// Command Name to class mapping dictionary
        /// </summary>
        public static Dictionary<string, string> commandNameToClassName = ConcatAllCommands();

        //Default values for the application
        /// <summary>
        /// Initial position of the drawer
        /// </summary>
        public static Point InitialPos = new Point(0, 0);
        /// <summary>
        /// Required linesplitter character
        /// </summary>
        public static char LineSplitter = '\n';
        /// <summary>
        /// Default value for shapefill
        /// </summary>
        public static bool ShapeFill = false;
        // Colors
        /// <summary>
        /// Default output canvas color
        /// </summary>
        public static Color OutputCanvasColor = Color.White;
        /// <summary>
        /// Default pen color
        /// </summary>
        public static Color PenColor = Color.Black;
        /// <summary>
        /// Default command color
        /// </summary>
        public static Color DefaultCommandColor = Color.Black;
        /// <summary>
        /// Default command background color
        /// </summary>
        public static Color DefaultCommandBackColor = Color.White;
        /// <summary>
        /// Default success color
        /// </summary>
        public static Color DefaultSuccessColor = Color.Green;
        /// <summary>
        /// Default Danger Color
        /// </summary>
        public static Color DefaultDangerColor = Color.Red;
        /// <summary>
        /// Default variable color
        /// </summary>
        public static Color DefaultVariableColor = Color.DeepSkyBlue;

        // Width
        /// <summary>
        /// Default pen width
        /// </summary>
        public static float PenWidth = 1;
        /// <summary>
        /// Default bitmap width and height
        /// </summary>
        public static int BITMAP_WIDTH = 640, BITMAP_HEIGHT = 480;
        /// <summary>
        /// Default width of the position cursor
        /// </summary>
        public static int PosWidth = 3;
        /// <summary>
        /// Default height of the tooltip
        /// </summary>
        public static int TooltipHeight = 32;



        // Available conditional statements
        /// <summary>
        /// Conditional statements list
        /// </summary>
        public static string[] conditionalStatements = new string[]
        {
            "if"
        };
        // Available Loop statements
        /// <summary>
        /// Loop statements list
        /// </summary>
        public static string[] loopStatements = new string[]
        {
            "while",
            "loop for"
        };

    }
}
