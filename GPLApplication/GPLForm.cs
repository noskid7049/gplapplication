﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GPLApplication
{
    /// <summary>
    /// Form for the main application
    /// </summary>
    public partial class GPLForm : Form
    {
        /// <summary>
        /// Outputs of the text boxes
        /// </summary>
        SyntaxParseOutput reqOutput, reqOutputSingleLine;
        /// <summary>
        /// Var to hold the current command in the single line command box
        /// </summary>
        string currentSingleLineCommand;
        /// <summary>
        /// Bool to check to change the current single command line var
        /// </summary>
        bool changeCurrentCommand;
        /// <summary>
        /// Required tooltip in the command boxes;
        /// </summary>
        ToolTip tip;
        /// <summary>
        /// Output Canvas store
        /// </summary>
        public OutputCanvas outputCanvas;
        /// <summary>
        /// Bitmap to store the drawn objects
        /// </summary>
        public Bitmap outputBitmap;
        /// <summary>
        /// Rectangle of the current position of the drawer in the output canvas
        /// </summary>
        public Rectangle posRect;
        /// <summary>
        /// Width of the position rectangle
        /// </summary>
        private int posWidth = AppProperties.PosWidth;
        /// <summary>
        /// History of the commands run in the single line command box
        /// </summary>
        public List<string> CommandHistory { set; get; }
        /// <summary>
        /// Current history index of the command
        /// </summary>
        public int HistoryIndex { set; get; }
        /// <summary>
        /// The constructor for the main form
        /// </summary>
        public GPLForm()
        {
            // Initialize components
            InitializeComponent();
            tip = new ToolTip();
            reqOutput = new SyntaxParseOutput(new List<CError> { }, new List<CSuccess> { }, new List<string>());
            reqOutputSingleLine = new SyntaxParseOutput(new List<CError> { }, new List<CSuccess> { }, new List<string>());
            outputBitmap = new Bitmap(AppProperties.BITMAP_WIDTH, AppProperties.BITMAP_HEIGHT);
            outputCanvas = new OutputCanvas(Graphics.FromImage(outputBitmap));
            // Initialize the command history list and the index of the history list
            CommandHistory = new List<string>();
            HistoryIndex = 0;
            currentSingleLineCommand = "";
            changeCurrentCommand = true;
            // Set the rectangle to the current position with appropriate width and height (width currently)
            posRect = new Rectangle(outputCanvas.CurrentPos, new Size(posWidth, posWidth));
            // Offset the rect to hold the cursor in center of cursor
            posRect.Offset(-posWidth / 2, -posWidth / 2);
            // Refresh the control
            Refresh();
        }
        /************************************************************
                Paint Event for the Picture Box
         *************************************************************/
        /// <summary>
        /// Event listener for the Output Picture Box Paint event
        /// Will draw the commands on the Output Bitmap
        /// </summary>
        /// <param name="sender">Event object</param>
        /// <param name="e">Paint event</param>
        private void OutputPictureBox_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            // Clear the screen on every paint object and draw the objects from the bitmap
            g.Clear(AppProperties.OutputCanvasColor);
            g.DrawImageUnscaled(outputBitmap, 0, 0);
            // Get the current location of the pointer in the current instance
            posRect.Location = outputCanvas.CurrentPos;
            // Offset the rectangle such that the center of the rect is the actual position 
            posRect.Offset(-posWidth / 2, -posWidth / 2);
            // Draw a rectangle for visual
            g.DrawRectangle(outputCanvas.Drawer, posRect);
        }
        /************************************************************
                Event Listeners for Run Button
         *************************************************************/
        /// <summary>
        /// Run button click event listener
        /// </summary>
        /// <param name="sender">Event object</param>
        /// <param name="e">Click Event object</param>
        private void StartButton_Click(object sender, EventArgs e)
        {
            try
            {
                RunProgram(MultiLineCommandBox, reqOutput);
            }
            catch (Exception exc)
            {
                Console.Write(exc.StackTrace);
                MessageBox.Show(exc.Message, "Error");
            }
        }
        /************************************************************
                Event Listeners for Multi Line Command Box
         *************************************************************/
        /// <summary>
        /// Text Change event listener for the multiline command box
        /// </summary>
        /// <param name="sender">Event sender Object</param>
        /// <param name="e">Event  object</param>
        private void MultiLineCommandBox_TextChanged(object sender, EventArgs e)
        {
            // Parse the command box with required keys in the config file
            SyntaxParseOutput output = CommandParser.ParseCommandBox(MultiLineCommandBox, AppProperties.commandNameToClassName.Keys);
            Console.WriteLine(output.ErrorList.Count);
            reqOutput = output;
            //Color the rich text box using the output
            ColorRichTextBox(MultiLineCommandBox, reqOutput);

            // giving back the focus
            MultiLineCommandBox.Focus();
        }
        /// <summary>
        /// Mouse move event listener for the multiline command box
        /// </summary>
        /// <param name="sender">Event Object</param>
        /// <param name="e">Mouse Event object</param>
        private void MultiLineCommandBox_MouseMove(object sender, MouseEventArgs e)
        {
            RenderErrorTooltip(e, MultiLineCommandBox, reqOutput);
        }
        /// <summary>
        /// Multiline Command Box focus event
        /// </summary>
        /// <param name="sender">Event sender Object</param>
        /// <param name="e">Mouse Event object</param>
        private void MultiLineCommandBox_Enter(object sender, EventArgs e)
        {
            MultiLineCommandBox.BackColor = Color.White;
            SingleLineCommandBox.BackColor = Color.Gray;
        }
        /************************************************************
                                    Timer
         *************************************************************/
        /// <summary>
        /// Timer for tip activation throttling
        /// </summary>
        /// <param name="sender">Timer which is used</param>
        /// <param name="e">Event object of the timer</param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
        }
        /************************************************************
                Event Listeners for Single Line Command Box
         *************************************************************/
        /// <summary>
        /// Single Line Command box text change event listener
        /// </summary>
        /// <param name="sender">Event sender Object</param>
        /// <param name="e">Event object</param>
        private void SingleLineCommandBox_TextChanged(object sender, EventArgs e)
        {
            if (changeCurrentCommand)
                currentSingleLineCommand = SingleLineCommandBox.Text;
            // Parse the command box with required keys in the config file
            SyntaxParseOutput output = CommandParser.ParseCommandBox(SingleLineCommandBox, AppProperties.commandNameToClassName.Keys);

            reqOutputSingleLine = output;
            //Color the rich text box using the output
            ColorRichTextBox(SingleLineCommandBox, reqOutputSingleLine);

            // giving back the focus
            SingleLineCommandBox.Focus();
        }
        /// <summary>
        /// Single line command box focus event listener
        /// </summary>
        /// <param name="sender">Event sender Object</param>
        /// <param name="e">Event object</param>
        private void SingleLineCommandBox_Enter(object sender, EventArgs e)
        {
            SingleLineCommandBox.BackColor = Color.White;
            MultiLineCommandBox.BackColor = Color.Gray;
        }
        /// <summary>
        /// Mouse move event listener for the single line command box
        /// </summary>
        /// <param name="sender">Event sender Object</param>
        /// <param name="e">Mouse Event object</param>
        private void SingleLineCommandBox_MouseMove(object sender, MouseEventArgs e)
        {
            RenderErrorTooltip(e, SingleLineCommandBox, reqOutputSingleLine);
        }
        /// <summary>
        /// Keyboard button pressed event listener for the Single Line Command Box 
        /// </summary>
        /// <param name="sender">Event sender Object</param>
        /// <param name="e">Key Event object</param>
        private void SingleLineCommandBox_KeyDown(object sender, KeyEventArgs e)
        {
            // Run the single line command box on enter press
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    // Run the program from the program window/ MultiLineCommandBox if "run" is input in the command window
                    string currText = SingleLineCommandBox.Text;
                    if (currText.Trim() == "run")
                    {
                        RunProgram(MultiLineCommandBox, reqOutput);
                    }
                    // Else run the current command in the command box
                    else
                    {
                        if (currText != "")
                        {
                            // Run the program
                            RunProgram(SingleLineCommandBox, reqOutputSingleLine);
                            CommandHistory.Add(reqOutputSingleLine.SuccessList[0].LineString);
                            HistoryIndex = CommandHistory.Count;
                            // Reset on every Enter press
                            reqOutputSingleLine = new SyntaxParseOutput(new List<CError> { }, new List<CSuccess> { }, new List<string>());
                        }
                    }
                    SingleLineCommandBox.Clear();
                    currentSingleLineCommand = "";
                }
                catch (Exception exc)
                {
                    Console.Write(exc.StackTrace);
                    MessageBox.Show(exc.Message, "Error");
                }
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Up)
            {
                changeCurrentCommand = false;
                // Reset on every Up press
                SingleLineCommandBox.Clear();
                reqOutputSingleLine = new SyntaxParseOutput(new List<CError> { }, new List<CSuccess> { }, new List<string>());
                // Decrease the history index and limit it to above zero
                HistoryIndex--;
                if (HistoryIndex <= 0) HistoryIndex = 0;
                // Replace the text from the history with the required index
                SingleLineCommandBox.Text = CommandHistory[HistoryIndex];
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Down)
            {
                changeCurrentCommand = false;
                // Reset on every Down press
                SingleLineCommandBox.Clear();
                reqOutputSingleLine = new SyntaxParseOutput(new List<CError> { }, new List<CSuccess> { }, new List<string>());
                // Increase the history index and limit it to the number of commands in the commands history
                HistoryIndex++;
                // Reset the history to the history cound and the text to the current input command in the single line command box
                if (HistoryIndex >= CommandHistory.Count)
                {
                    HistoryIndex = CommandHistory.Count;
                    // Replace the text from the history with the required index
                    SingleLineCommandBox.Text = currentSingleLineCommand;
                    changeCurrentCommand = true;
                }
                else
                {
                    SingleLineCommandBox.Text = CommandHistory[HistoryIndex];
                }
                e.Handled = true;
            }
        }
        /************************************************************
                        Event Listeners for Menu Strip
         *************************************************************/
        /// <summary>
        /// Event listener for the save button click in the MenuStrip
        /// </summary>
        /// <param name="sender">Event sender Object</param>
        /// <param name="e">Event object</param>
        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // For disposing the dialog box
            using (var sfd = new SaveFileDialog())
            {
                sfd.Filter = "txt files (*.txt)|*.txt";
                sfd.FilterIndex = 1;

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(sfd.FileName, MultiLineCommandBox.Text);
                }
            }
        }
        /// <summary>
        /// Event listener for the load button click in the MenuStrip
        /// </summary>
        /// <param name="sender">Event sender Object</param>
        /// <param name="e">Event object</param>
        private void LoadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // For disposing the dialog box
            using (var ofd = new OpenFileDialog()
            {
                FileName = "Select a text file",
                Filter = "Text files (*.txt)|*.txt",
                Title = "Open text file"
            })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    string s = File.ReadAllText(ofd.FileName);
                    MultiLineCommandBox.Text = s;
                }
            }
        }
        /************************************************************
                                Helper Functions
         *************************************************************/
        /// <summary>
        /// Function to color the box according to the passed output
        /// </summary>
        /// <param name="box">Box to use and color</param>
        /// <param name="output">Required output</param>
        private void ColorRichTextBox(RichTextBox box, SyntaxParseOutput output)
        {
            // saving the original caret position + forecolor
            int originalIndex = box.SelectionStart;
            int originalLength = box.SelectionLength;
            Color originalColor = AppProperties.DefaultCommandColor;

            // removes any previous highlighting (so modified words won't remain highlighted)
            box.SelectionStart = 0;
            box.SelectionLength = box.Text.Length;
            box.SelectionColor = originalColor;
            //Loop through the error list
            output.ErrorList.ForEach(error =>
            {
                box.SelectionStart = error.Index;
                box.SelectionLength = error.Length;
                box.SelectionColor = AppProperties.DefaultDangerColor;
            });
            //Loop through the success list
            output.SuccessList.ForEach(success =>
            {
                // Color the whole line matching the syntax
                box.SelectionStart = success.Index;
                box.SelectionLength = success.Length;
                box.SelectionColor = AppProperties.DefaultSuccessColor;
            });
            output.Variables.ForEach(variable =>
            {
                Regex r = new Regex(@"(?<=^([^""]| ""[^""]* "")*)\b" + variable + @"\b");
                MatchCollection matches = r.Matches(box.Text);
                foreach (Match match in matches)
                {
                    // Color the whole line matching the syntax
                    box.SelectionStart = match.Index;
                    box.SelectionLength = match.Length;
                    box.SelectionColor = AppProperties.DefaultVariableColor;
                }
            });

            // restoring the original colors, for further writing
            box.SelectionStart = originalIndex;
            box.SelectionLength = originalLength;
            box.SelectionColor = originalColor;
        }
        /// <summary>
        /// Function to render error tooltip on hover on the richtextbox passed
        /// </summary>
        /// <param name="e">Mouse event object</param>
        /// <param name="box">Box to check on</param>
        /// <param name="output">Required output</param>
        private void RenderErrorTooltip(MouseEventArgs e, RichTextBox box, SyntaxParseOutput output)
        {
            // timer used for throttling the tooltip draw event
            if (!timer1.Enabled)
            {
                //Get the current mouse position
                Point curr_pos = new Point(e.X, e.Y);
                //Get the current char index from the mouse position
                int posToSearch = box.GetCharIndexFromPosition(curr_pos);
                bool errorFound = false;
                // Check if the hovered text is part of the error list
                output.ErrorList.ForEach(error =>
                {
                    if (posToSearch >= error.Index && posToSearch <= error.Index + error.Length)
                    {
                        // Set the title to the error text of the current error
                        tip.ToolTipTitle = error.ErrTxt;
                        // Location of the command box
                        Point p = box.Location;
                        tip.Show(error.PossibleFix, this, p.X + e.X,
                            p.Y + e.Y + AppProperties.TooltipHeight,
                            1000);
                        timer1.Enabled = true;
                        errorFound = true;
                    }
                });
                // Activate the tooltip if the hovered text has error
                if (!errorFound)
                {
                    tip.Active = false;
                }
                else
                {
                    tip.Active = true;
                }
            }
        }
        /// <summary>
        /// Exit button click
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event param</param>
        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        /// <summary>
        /// About click
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event param</param>
        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show($"Author: Dikson Rajbanshi \nCopyright©2020", "About");
        }

        private void CheckSyntaxBtn_Click(object sender, EventArgs e)
        {
            try
            {
                RunProgram(MultiLineCommandBox, reqOutput, false);
            }
            catch (Exception exc)
            {
                Console.Write(exc.StackTrace);
                MessageBox.Show(exc.Message, "Error");
            }
        }


        /// <summary>
        /// Function to run the program
        /// </summary>
        /// <param name="box">Required box to run the program from</param>
        /// <param name="output">Parse output to run the program on</param>
        /// <param name="runCommand">Bool to check if the command should run</param>
        private void RunProgram(RichTextBox box, SyntaxParseOutput output, bool runCommand = true)
        {
            // Run only if the text has no error
            if (output.ErrorList.Count == 0)
            {
                // List of lines of successful commands
                box.Text = box.Text.Trim();
                List<string> commands = new List<string>(box.Text.Split('\n'));

                try
                {
                    if (box.Text != "")
                        CommandParser.RunMultiLineCommands(commands, outputCanvas, runCommand);
                    Refresh();
                }
                catch (Exception exp)
                {
                    throw new Exception($"Error: {exp.Message}");
                }
            }
            else
            {
                string errorMsg = "";
                output.ErrorList.ForEach(error =>
                {
                    int lineNum = MultiLineCommandBox.GetLineFromCharIndex(error.Index);
                    errorMsg += $"{error.ErrTxt} txt : Line {lineNum} \n";
                });
                throw new Exception("Please fix the errors shown: \n" + errorMsg);
            }
            box.Focus();
        }


    }
}
