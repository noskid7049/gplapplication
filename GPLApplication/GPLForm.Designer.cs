﻿namespace GPLApplication
{
    partial class GPLForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.OutputPictureBox = new System.Windows.Forms.PictureBox();
            this.MultiLineCommandBox = new System.Windows.Forms.RichTextBox();
            this.StartButton = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SingleLineCommandBox = new System.Windows.Forms.RichTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CheckSyntaxBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.OutputPictureBox)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // OutputPictureBox
            // 
            this.OutputPictureBox.BackColor = System.Drawing.Color.White;
            this.OutputPictureBox.Location = new System.Drawing.Point(239, 49);
            this.OutputPictureBox.Name = "OutputPictureBox";
            this.OutputPictureBox.Size = new System.Drawing.Size(549, 394);
            this.OutputPictureBox.TabIndex = 0;
            this.OutputPictureBox.TabStop = false;
            this.OutputPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.OutputPictureBox_Paint);
            // 
            // MultiLineCommandBox
            // 
            this.MultiLineCommandBox.AcceptsTab = true;
            this.MultiLineCommandBox.BackColor = System.Drawing.Color.DarkGray;
            this.MultiLineCommandBox.Location = new System.Drawing.Point(12, 49);
            this.MultiLineCommandBox.Name = "MultiLineCommandBox";
            this.MultiLineCommandBox.Size = new System.Drawing.Size(210, 291);
            this.MultiLineCommandBox.TabIndex = 1;
            this.MultiLineCommandBox.Text = "";
            this.MultiLineCommandBox.TextChanged += new System.EventHandler(this.MultiLineCommandBox_TextChanged);
            this.MultiLineCommandBox.Enter += new System.EventHandler(this.MultiLineCommandBox_Enter);
            this.MultiLineCommandBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MultiLineCommandBox_MouseMove);
            // 
            // StartButton
            // 
            this.StartButton.Location = new System.Drawing.Point(12, 405);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(95, 38);
            this.StartButton.TabIndex = 2;
            this.StartButton.Text = "Run";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // SingleLineCommandBox
            // 
            this.SingleLineCommandBox.BackColor = System.Drawing.Color.DarkGray;
            this.SingleLineCommandBox.Location = new System.Drawing.Point(12, 359);
            this.SingleLineCommandBox.Multiline = false;
            this.SingleLineCommandBox.Name = "SingleLineCommandBox";
            this.SingleLineCommandBox.Size = new System.Drawing.Size(210, 29);
            this.SingleLineCommandBox.TabIndex = 3;
            this.SingleLineCommandBox.Text = "";
            this.SingleLineCommandBox.TextChanged += new System.EventHandler(this.SingleLineCommandBox_TextChanged);
            this.SingleLineCommandBox.Enter += new System.EventHandler(this.SingleLineCommandBox_Enter);
            this.SingleLineCommandBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SingleLineCommandBox_KeyDown);
            this.SingleLineCommandBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SingleLineCommandBox_MouseMove);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.LoadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.SaveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
            // 
            // CheckSyntaxBtn
            // 
            this.CheckSyntaxBtn.Location = new System.Drawing.Point(127, 405);
            this.CheckSyntaxBtn.Name = "CheckSyntaxBtn";
            this.CheckSyntaxBtn.Size = new System.Drawing.Size(95, 38);
            this.CheckSyntaxBtn.TabIndex = 5;
            this.CheckSyntaxBtn.Text = "Check Syntax";
            this.CheckSyntaxBtn.UseVisualStyleBackColor = true;
            this.CheckSyntaxBtn.Click += new System.EventHandler(this.CheckSyntaxBtn_Click);
            // 
            // GPLForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 466);
            this.Controls.Add(this.CheckSyntaxBtn);
            this.Controls.Add(this.SingleLineCommandBox);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.MultiLineCommandBox);
            this.Controls.Add(this.OutputPictureBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "GPLForm";
            this.Text = "GPL Application";
            ((System.ComponentModel.ISupportInitialize)(this.OutputPictureBox)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox OutputPictureBox;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.RichTextBox SingleLineCommandBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.RichTextBox MultiLineCommandBox;
        private System.Windows.Forms.Button CheckSyntaxBtn;
    }
}

