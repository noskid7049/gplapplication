﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// Exception for Syntax errors
    /// </summary>
    [Serializable]
    public class SyntaxException : Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public SyntaxException() : base("Some Error Occured")
        {
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="err">Required Syntax error</param>
        public SyntaxException(string err) : base(err) { }
    }
}
