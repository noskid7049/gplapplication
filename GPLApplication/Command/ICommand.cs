﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace GPLApplication
{
    public interface ICommand
    {
        /// <summary>
        /// Name of the command
        /// </summary>
        string CommandName();
        /// <summary>
        /// Syntax in string form for display
        /// </summary>
        Syntax[] CommandSyntaxArr();
        /// <summary>
        /// Getter for the currently used syntax of the command object
        /// </summary>
        /// <returns>Currrent Syntax used in the command object</returns>
        Syntax CommandCurrSyntax();
        /// <summary>
        /// Current Syntax Setter
        /// </summary>
        /// <param name="CurrSyntax">Syntax to be set</param>
        void CommandCurrSyntax(Syntax CurrSyntax);
        /// <summary>
        /// OutputCanvas used Getter
        /// </summary>
        /// <returns>Currently used Canvas</returns>
        OutputCanvas CommandOC();
        /// <summary>
        /// OutputCanvas to be used setter
        /// </summary>
        /// <param name="OC">OutputCanvas to be used</param>
        void CommandOC(OutputCanvas OC);

        /// <summary>
        /// Run the command of the current object
        /// </summary>
        void Run();
        /// <summary>
        /// Getter of Command Params
        /// </summary>
        /// <returns>Array of params</returns>
        //String[] CommandParameters();
        /// <summary>
        /// Parameter setter for the object
        /// </summary>
        /// <param name="paramsArr">Parameter Array in the command</param>
        void SetParams(string[] paramsArr);
    }
}
