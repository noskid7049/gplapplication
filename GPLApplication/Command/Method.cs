﻿using CodingSeb.ExpressionEvaluator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// Method Command class
    /// </summary>
    public class Method : ICommand
    {
        /// <summary>
        /// Name of the command
        /// </summary>
        public const string Name = "method";
        /// <summary>
        /// Object containing the string and regex of the syntax allowed for the command
        /// </summary>
        public static readonly Syntax[] SyntaxArr = new[]{
            new Syntax("<methodName>(<paramList>)", RegexObject.regexMethodCall)
        };
        /// <summary>
        /// The current syntax used in the command object
        /// </summary>
        public Syntax CurrSyntax { get; set; }
        /// <summary>
        /// OutputCanvas to be used for the command
        /// </summary>
        public OutputCanvas OC { set; get; }
        /// <summary>
        /// The name of the method
        /// </summary>
        public string MethodName { set; get; }
        /// <summary>
        /// List of the parameter variable names defined
        /// </summary>
        public string[] ParamList { set; get; }
        /// <summary>
        /// List of commands for the current method
        /// </summary>
        public List<string> CommandList { set; get; }
        /// <summary>
        /// Integer value to determine which setter and process command to use for the run command
        /// </summary>
        private int ParamType { set; get; }
        /// <summary>
        /// Constructor
        /// </summary>
        public Method()
        {
            MethodName = "";
            ParamList = new string[0];
            CurrSyntax = SyntaxArr[0];
            CommandList = new List<string>();
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="methodName">Name of the method</param>
        /// <param name="paramList">Param list</param>
        /// <param name="commandList">Commands list</param>
        public Method(string methodName, string[] paramList, List<string> commandList) : this()
        {
            MethodName = methodName;
            ParamList = paramList;
            CommandList = commandList;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="methodName">Name of the method</param>
        /// <param name="paramList">list of params</param>
        /// <param name="commandList">List of commands</param>
        /// <param name="OC">Output Canvas for the method</param>
        public Method(string methodName, string[] paramList, List<string> commandList, OutputCanvas OC) : this()
        {
            MethodName = methodName;
            ParamList = paramList;
            this.OC = OC;
        }
        public string CommandName()
        {
            return Name;
        }

        public Syntax[] CommandSyntaxArr()
        {
            return SyntaxArr;
        }

        public Syntax CommandCurrSyntax()
        {
            return CurrSyntax;
        }

        public void CommandCurrSyntax(Syntax CurrSyntax)
        {
            this.CurrSyntax = CurrSyntax;
        }

        public OutputCanvas CommandOC()
        {
            return OC;
        }

        public void CommandOC(OutputCanvas OC)
        {
            this.OC = OC;
        }

        public void SetParams(string[] paramsArr)
        {
            ParamType = Array.FindIndex<Syntax>(SyntaxArr, row => row.SyntaxString == CurrSyntax.SyntaxString);
            //Check for multipleswitch cases at once
            switch (ParamType)
            {
                case var s when new[] { 0 }.Contains(s):
                    try
                    {
                        ParamList = paramsArr;
                    }
                    catch (Exception e)
                    {
                        throw new SyntaxException(e.Message);
                    }
                    break;
            }
        }
        public void Run()
        {
            throw new Exception("Depricated: Please use the Run function with arguments string");
        }
        /// <summary>
        /// Run the method commands with the given arguments values
        /// </summary>
        /// <param name="argsString">Argument string to use as the method parameter values</param>
        public void Run(string argsString)
        {
            // Split the values by ","
            string[] argsList = Helper.SplitString(argsString.Trim(), ",");
            // Throw error if the parameter list length and the length of the values paseed is not equal
            if (ParamList.Length != argsList.Length)
            {
                throw new SyntaxException("Syntax Error. Please input the equal number of parameters as in the definer");
            }
            // Try to parse the passed values to int
            try
            {
                Console.WriteLine(argsList.Length);
                for (int i = 0; i < argsList.Length; i++)
                {
                    if (ParamList[i] != "" || argsList[i] != "")
                        OC.Evaluator.Variables[ParamList[i]] = int.Parse(argsList[i]);
                }
                CommandParser.RunMultiLineCommands(this.CommandList, this.OC);
            }
            catch (Exception err)
            {
                throw new SyntaxException($"Error while running method: {MethodName}\n{err.Message}");
            }
        }
    }
}
