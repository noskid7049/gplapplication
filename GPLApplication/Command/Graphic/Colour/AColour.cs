﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// Abstract class for the colour type commands
    /// </summary>
    public abstract class AColour : IGraphic
    {
        /// <inheritdoc />
        public abstract Syntax CommandCurrSyntax();
        /// <inheritdoc />
        public abstract void CommandCurrSyntax(Syntax CurrSyntax);
        /// <inheritdoc />
        public abstract string CommandName();
        /// <inheritdoc />
        public abstract OutputCanvas CommandOC();
        /// <inheritdoc />
        public abstract void CommandOC(OutputCanvas OC);
        /// <inheritdoc />
        public abstract Syntax[] CommandSyntaxArr();
        /// <inheritdoc />
        public abstract void Run(OutputCanvas oc);
        /// <inheritdoc />
        public abstract void Run();
        /// <inheritdoc />
        public abstract void SetParams(string[] paramsArr);
    }
}
