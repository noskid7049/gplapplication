﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// Fill Command class
    /// </summary>
    public class Fill : AColour
    {
        /// <summary>
        /// Name of the command
        /// </summary>
        public const string Name = "fill";
        /// <summary>
        /// Object containing the string and regex of the syntax allowed for the command
        /// </summary>
        public static readonly Syntax[] SyntaxArr = new[]{
            new Syntax(Name, new Regex(@"\b(?<commandName>" + Name +@")\b\s"+ @"(?<commandParams>(?:on|off))"))
        };
        /// <summary>
        /// The current syntax used in the command object
        /// </summary>
        public Syntax CurrSyntax { get; set; }
        /// <summary>
        /// OutputCanvas to be used for the command
        /// </summary>
        public OutputCanvas OC { set; get; }
        /// <summary>
        /// Integer value to determine which setter and process command to use for the run command
        /// </summary>
        private int ParamType { set; get; }
        /// <summary>
        /// Required Shape Fill for succeding Shape commands
        /// </summary>
        public bool ShapeFill { set; get; }
        /// <summary>
        /// Constructor
        /// </summary>
        public Fill()
        {
            this.CurrSyntax = SyntaxArr[0];
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="status">Required Shape Fill Status</param>
        public Fill(string status) : this()
        {
            if (status == "on")
            {
                ShapeFill = true;
            }
            else if (status == "false")
            {
                ShapeFill = true;
            }
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="status">Required Shape Fill Status</param>
        /// <param name="OC">OutputCanvas to use</param>
        public Fill(string status, OutputCanvas OC) : this()
        {
            this.OC = OC;
            if (status == "on")
            {
                ShapeFill = true;
            }
            else if (status == "false")
            {
                ShapeFill = true;
            }
        }
        public override Syntax CommandCurrSyntax()
        {
            return CurrSyntax;
        }

        public override void CommandCurrSyntax(Syntax CurrSyntax)
        {
            this.CurrSyntax = CurrSyntax;
        }

        public override string CommandName()
        {
            return Name;
        }

        public override OutputCanvas CommandOC()
        {
            return OC;
        }

        public override void CommandOC(OutputCanvas OC)
        {
            this.OC = OC;
        }

        public override Syntax[] CommandSyntaxArr()
        {
            return SyntaxArr;
        }

        public override void Run()
        {
            switch (ParamType)
            {
                case var s when new[] { 0 }.Contains(s):
                    OC.ShapeFill = ShapeFill;
                    break;
            }
        }

        public override void SetParams(string[] paramsArr)
        {
            ParamType = Array.FindIndex<Syntax>(SyntaxArr, row => row.SyntaxString == CurrSyntax.SyntaxString);
            switch (ParamType)
            {
                case var s when new[] { 0 }.Contains(s):
                    string status = paramsArr[0].ToLower();
                    bool fill;
                    switch (status)
                    {
                        case "on":
                            fill = true;
                            break;
                        case "off":
                            fill = false;
                            break;
                        default:
                            throw new SyntaxException("Wrong Param input");
                    }
                    ShapeFill = fill;
                    break;
            }
        }

        public override void Run(OutputCanvas oc)
        {
            this.OC = oc;
            Run();
        }
    }
}
