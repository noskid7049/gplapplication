﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Drawing;

namespace GPLApplication
{
    /// <summary>
    /// Pen Command Class
    /// </summary>
    public class PenC : AColour
    {
        /// <summary>
        /// Name of the command
        /// </summary>
        public const string Name = "pen";
        /// <summary>
        /// Object containing the string and regex of the syntax allowed for the command
        /// </summary>
        public static readonly Syntax[] SyntaxArr = new[]{
            new Syntax(Name, new Regex(@"^\b(?<commandName>" + Name +@")\b\s{1}(?<commandParams>[a-zA-Z]+)$"))
        };
        /// <summary>
        /// The current syntax used in the command object
        /// </summary>
        public Syntax CurrSyntax { get; set; }
        /// <summary>
        /// OutputCanvas to be used for the command
        /// </summary>
        public OutputCanvas OC { set; get; }
        /// <summary>
        /// Integer value to determine which setter and process command to use for the run command
        /// </summary>
        private int ParamType { set; get; }
        /// <summary>
        /// Color of the pen
        /// </summary>
        public Color PenColor { set; get; }
        /// <summary>
        /// Constructor
        /// </summary>
        public PenC()
        {
            this.CurrSyntax = SyntaxArr[0];
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="color">Color of the pen</param>
        public PenC(Color color) : this()
        {
            this.PenColor = color;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="color">Color of the pen</param>
        /// <param name="OC">OutputCanvas to use</param>
        public PenC(Color color, OutputCanvas OC) : this()
        {
            this.PenColor = color;
            this.OC = OC;
        }
        public override Syntax CommandCurrSyntax()
        {
            return CurrSyntax;
        }

        public override void CommandCurrSyntax(Syntax CurrSyntax)
        {
            this.CurrSyntax = CurrSyntax;
        }

        public override string CommandName()
        {
            return Name;
        }

        public override OutputCanvas CommandOC()
        {
            return OC;
        }

        public override void CommandOC(OutputCanvas OC)
        {
            this.OC = OC;
        }

        public override Syntax[] CommandSyntaxArr()
        {
            return SyntaxArr;
        }

        public override void Run()
        {
            switch (ParamType)
            {
                case var s when new[] { 0 }.Contains(s):
                    //AppProperties.PenColor = PenColor;
                    OC.Drawer.Color = PenColor;
                    break;
            }
        }

        public override void SetParams(string[] paramsArr)
        {
            ParamType = Array.FindIndex<Syntax>(SyntaxArr, row => row.SyntaxString == CurrSyntax.SyntaxString);
            switch (ParamType)
            {
                case var s when new[] { 0 }.Contains(s):
                    string ColorStr = paramsArr[0].ToLower();
                    Color reqColor;
                    switch (ColorStr)
                    {
                        case "red":
                            reqColor = Color.Red;
                            break;
                        case "green":
                            reqColor = Color.Green;
                            break;
                        case "black":
                            reqColor = Color.Black;
                            break;
                        case "blue":
                            reqColor = Color.Blue;
                            break;
                        case "brown":
                            reqColor = Color.Brown;
                            break;
                        case "gold":
                            reqColor = Color.Gold;
                            break;
                        default:
                            throw new SyntaxException("Color Not Allowed");
                    }
                    PenColor = reqColor;
                    break;
            }
        }

        public override void Run(OutputCanvas oc)
        {
            this.OC = oc;
            Run();
        }
    }
}
