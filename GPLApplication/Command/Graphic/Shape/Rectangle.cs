﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// Rectangle command class
    /// </summary>
    public class RectangleC : AShape
    {
        /// <summary>
        /// Name of the command
        /// </summary>
        public const string Name = "rect";
        /// <summary>
        /// Object containing the string and regex of the syntax allowed for the command
        /// </summary>
        public static readonly Syntax[] SyntaxArr = new[]{
            new Syntax(Name+" W,H", new Regex(@"^\b(?<commandName>" + Name + @")\b\s{1}(?<commandParams>\w+\s*,\s*\w+)$"))
        };
        /// <summary>
        /// The current syntax used in the command object
        /// </summary>
        public Syntax CurrSyntax { get; set; }
        /// <summary>
        /// The width for the command
        /// </summary>
        public float Width { set; get; }
        /// <summary>
        /// The Height for the command
        /// </summary>
        public float Height { set; get; }
        /// <summary>
        /// OutputCanvas to be used for the command
        /// </summary>
        public OutputCanvas OC { set; get; }
        /// <summary>
        /// Integer value to determine which setter and process command to use for the run command
        /// </summary>
        private int ParamType { set; get; }
        /// <summary>
        /// Constructor
        /// </summary>
        public RectangleC()
        {
            this.CurrSyntax = SyntaxArr[0];
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="width">Width of the rectangle</param>
        /// <param name="height">Height of the rectangle</param>
        public RectangleC(float width, float height) : this()
        {
            Width = width;
            Height = height;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="width">Width of the rectangle</param>
        /// <param name="height">Height of the rectangle</param>
        /// <param name="OC">Output Canvas to use</param>
        public RectangleC(float width, float height, OutputCanvas OC) : this()
        {
            Width = width;
            Height = height;
            this.OC = OC;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paramsArr">String of the parameters in the command</param>
        public RectangleC(string[] paramsArr) : this()
        {
            SetParams(paramsArr);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paramsArr">String of the parameters in the command</param>
        /// <param name="OC">Output Canvas to use</param>
        public RectangleC(string[] paramsArr, OutputCanvas OC) : this()
        {
            SetParams(paramsArr);
            this.OC = OC;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="width">Width of the rectangle</param>
        /// <param name="height">Height of the rectangle</param>
        /// <param name="CurrSyntax">Syntax object of the command used to invoke the object</param>
        public RectangleC(float width, float height, Syntax CurrSyntax)
        {
            this.CurrSyntax = CurrSyntax;
            Width = width;
            Height = height;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="width">Width of the rectangle</param>
        /// <param name="height">Height of the rectangle</param>
        /// <param name="CurrSyntax">Syntax object of the command used to invoke the object</param>
        /// <param name="OC">Output Canvas to use</param>
        public RectangleC(float width, float height, Syntax CurrSyntax, OutputCanvas OC)
        {
            this.CurrSyntax = CurrSyntax;
            Width = width;
            Height = height;
            this.OC = OC;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paramsArr">Parameter Array in the command</param>
        /// <param name="CurrSyntax">Syntax object of the command used to invoke the object</param>
        public RectangleC(string[] paramsArr, Syntax CurrSyntax)
        {
            this.CurrSyntax = CurrSyntax;
            SetParams(paramsArr);
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paramsArr">Parameter Array in the command</param>
        /// <param name="CurrSyntax">Syntax object of the command used to invoke the object</param>
        /// <param name="OC">Output Canvas to use</param>
        public RectangleC(string[] paramsArr, Syntax CurrSyntax, OutputCanvas OC)
        {
            this.CurrSyntax = CurrSyntax;
            SetParams(paramsArr);
            this.OC = OC;
        }
        /// <summary>
        /// Parameter setter for the object
        /// </summary>
        /// <param name="paramsArr">Parameter Array in the command</param>
        public override void SetParams(string[] paramsArr)
        {
            ParamType = Array.FindIndex<Syntax>(SyntaxArr, row => row.SyntaxString == CurrSyntax.SyntaxString);
            //Check for multipleswitch cases at once
            switch (ParamType)
            {
                case var s when new[] { 0 }.Contains(s):
                    try
                    {
                        Width = OC.Evaluator.Evaluate<int>(paramsArr[0]);
                        Height = OC.Evaluator.Evaluate<int>(paramsArr[1]);
                    }
                    catch (Exception e)
                    {
                        throw new SyntaxException(e.Message);
                    }
                    break;

            }
        }

        public override void Run()
        {
            // Offset the X and Y of the rectangle to center the rectangle to the current position
            int X = OC.CurrentPos.X - (int)(Width / 2);
            int Y = OC.CurrentPos.Y - (int)(Height / 2);
            if (!OC.ShapeFill)
            {
                switch (ParamType)
                {
                    case var s when new[] { 0 }.Contains(s):
                        OC.g.DrawRectangle(OC.Drawer, X, Y, Width, Height);
                        break;
                }
            }
            else
            {
                SolidBrush reqBrush = new SolidBrush(OC.Drawer.Color);
                switch (ParamType)
                {
                    case var s when new[] { 0 }.Contains(s):
                        OC.g.FillRectangle(reqBrush, X, Y, Width, Height);
                        break;
                }
            }
        }
        public override void Run(OutputCanvas OC)
        {
            this.OC = OC;
            Run();
        }

        public override string CommandName()
        {
            return Name;
        }

        public override Syntax[] CommandSyntaxArr()
        {
            return SyntaxArr;
        }

        public override Syntax CommandCurrSyntax()
        {
            return CurrSyntax;
        }

        public override void CommandCurrSyntax(Syntax CurrSyntax)
        {
            this.CurrSyntax = CurrSyntax;
        }

        public override OutputCanvas CommandOC()
        {
            return OC;
        }

        public override void CommandOC(OutputCanvas OC)
        {
            this.OC = OC;
        }
    }
}
