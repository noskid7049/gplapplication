﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// Circle command class
    /// </summary>
    public class Circle : AShape
    {
        /// <summary>
        /// Name of the command
        /// </summary>
        public const string Name = "circle";
        /// <summary>
        /// Object containing the string and regex of the syntax allowed for the command
        /// </summary>
        public static readonly Syntax[] SyntaxArr = new[]{
            new Syntax(Name+" W", new Regex(@"^\b(?<commandName>" + Name + @")\b\s{1}(?<commandParams>\w+)$"))
        };
        /// <summary>
        /// The current syntax used in the command object
        /// </summary>
        public Syntax CurrSyntax { get; set; }
        /// <summary>
        /// The radius for the command
        /// </summary>
        public float Radius { set; get; }
        /// <summary>
        /// OutputCanvas to be used for the command
        /// </summary>
        public OutputCanvas OC { set; get; }
        /// <summary>
        /// Integer value to determine which setter and process command to use for the run command
        /// </summary>
        private int ParamType { set; get; }
        /// <summary>
        /// Constructor
        /// </summary>
        public Circle()
        {
            this.CurrSyntax = SyntaxArr[0];
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="radius">Radius of the circle</param>
        public Circle(float radius) : this()
        {
            Radius = radius;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="radius">Radius of the circle</param>
        /// <param name="OC">Output Canvas to use</param>
        public Circle(float radius, OutputCanvas OC) : this()
        {
            Radius = radius;
            this.OC = OC;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paramsArr">String of the parameters in the command</param>
        public Circle(string[] paramsArr) : this()
        {
            SetParams(paramsArr);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paramsArr">String of the parameters in the command</param>
        /// <param name="OC">Output Canvas to use</param>
        public Circle(string[] paramsArr, OutputCanvas OC) : this()
        {
            SetParams(paramsArr);
            this.OC = OC;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="radius">Radius of the circle</param>
        /// <param name="CurrSyntax">Syntax object of the command used to invoke the object</param>
        public Circle(float radius, Syntax CurrSyntax)
        {
            this.CurrSyntax = CurrSyntax;
            Radius = radius;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="radius">Radius of the circle</param>
        /// <param name="CurrSyntax">Syntax object of the command used to invoke the object</param>
        /// <param name="OC">Output Canvas to use</param>
        public Circle(float radius, Syntax CurrSyntax, OutputCanvas OC)
        {
            this.CurrSyntax = CurrSyntax;
            Radius = radius;
            this.OC = OC;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paramsArr">Parameter Array in the command</param>
        /// <param name="CurrSyntax">Syntax object of the command used to invoke the object</param>
        public Circle(string[] paramsArr, Syntax CurrSyntax)
        {
            this.CurrSyntax = CurrSyntax;
            SetParams(paramsArr);
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paramsArr">Parameter Array in the command</param>
        /// <param name="CurrSyntax">Syntax object of the command used to invoke the object</param>
        /// <param name="OC">Output Canvas to use</param>
        public Circle(string[] paramsArr, Syntax CurrSyntax, OutputCanvas OC)
        {
            this.CurrSyntax = CurrSyntax;
            SetParams(paramsArr);
            this.OC = OC;
        }
        /// <summary>
        /// Parameter setter for the object
        /// </summary>
        /// <param name="paramsArr">Parameter Array in the command</param>
        public override void SetParams(string[] paramsArr)
        {
            ParamType = Array.FindIndex<Syntax>(SyntaxArr, row => row.SyntaxString == CurrSyntax.SyntaxString);
            //Check for multipleswitch cases at once
            switch (ParamType)
            {
                case var s when new[] { 0 }.Contains(s):
                    try
                    {
                        Radius = OC.Evaluator.Evaluate<int>(paramsArr[0]);
                    }
                    catch (Exception e)
                    {
                        throw new SyntaxException(e.Message);
                    }
                    break;
            }
        }

        public override void Run()
        {
            int X = OC.CurrentPos.X - (int)Radius;
            int Y = OC.CurrentPos.Y - (int)Radius;
            if (!OC.ShapeFill)
            {
                switch (ParamType)
                {
                    case var s when new[] { 0 }.Contains(s):
                        OC.g.DrawEllipse(OC.Drawer, new Rectangle(X, Y, (int)Radius * 2, (int)Radius * 2));
                        break;
                }
            }
            else
            {
                SolidBrush reqBrush = new SolidBrush(OC.Drawer.Color);
                switch (ParamType)
                {
                    case var s when new[] { 0 }.Contains(s):
                        OC.g.FillEllipse(reqBrush, new Rectangle(X, Y, (int)Radius * 2, (int)Radius * 2));
                        break;
                }
            }
        }
        public override void Run(OutputCanvas OC)
        {
            this.OC = OC;
            Run();
        }

        public override string CommandName()
        {
            return Name;
        }

        public override Syntax[] CommandSyntaxArr()
        {
            return SyntaxArr;
        }

        public override Syntax CommandCurrSyntax()
        {
            return CurrSyntax;
        }

        public override void CommandCurrSyntax(Syntax CurrSyntax)
        {
            this.CurrSyntax = CurrSyntax;
        }

        public override OutputCanvas CommandOC()
        {
            return OC;
        }

        public override void CommandOC(OutputCanvas OC)
        {
            this.OC = OC;
        }
    }
}
