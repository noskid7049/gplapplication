﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// Rectangle command class
    /// </summary>
    public class Polygon : AShape
    {
        /// <summary>
        /// Name of the command
        /// </summary>
        public const string Name = "polygon";
        /// <summary>
        /// Object containing the string and regex of the syntax allowed for the command
        /// </summary>
        public static readonly Syntax[] SyntaxArr = new[]{
            new Syntax(Name+" W,H", new Regex(@"^(?<commandName>\b"+Name+@"\b)\s+(?<commandParams>(\(\w+\s*,\s*\w+\),)*(\(\w+\s*,\s*\w+\)))$"))
        };
        /// <summary>
        /// The current syntax used in the command object
        /// </summary>
        public Syntax CurrSyntax { get; set; }
        /// <summary>
        /// List of points of the polygon
        /// </summary>
        public List<Point> PointList { get; set; }
        /// <summary>
        /// OutputCanvas to be used for the command
        /// </summary>
        public OutputCanvas OC { set; get; }
        /// <summary>
        /// Integer value to determine which setter and process command to use for the run command
        /// </summary>
        private int ParamType { set; get; }
        /// <summary>
        /// Constructor
        /// </summary>
        public Polygon()
        {
            this.CurrSyntax = SyntaxArr[0];
            PointList = new List<Point>();
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="PointList">List of points</param>
        public Polygon(List<Point> PointList) : this()
        {
            this.PointList = PointList;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="PointList">List of points</param>
        /// <param name="OC">Output Canvas to use</param>
        public Polygon(List<Point> PointList, OutputCanvas OC) : this()
        {
            this.PointList = PointList;
            this.OC = OC;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paramsArr">String of the parameters in the command</param>
        public Polygon(string[] paramsArr) : this()
        {
            SetParams(paramsArr);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paramsArr">String of the parameters in the command</param>
        /// <param name="OC">Output Canvas to use</param>
        public Polygon(string[] paramsArr, OutputCanvas OC) : this()
        {
            SetParams(paramsArr);
            this.OC = OC;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="PointList">List of points</param>
        /// <param name="CurrSyntax">Syntax object of the command used to invoke the object</param>
        public Polygon(List<Point> PointList, Syntax CurrSyntax)
        {
            this.CurrSyntax = CurrSyntax;
            this.PointList = PointList;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="PointList">List of points</param>
        /// <param name="CurrSyntax">Syntax object of the command used to invoke the object</param>
        /// <param name="OC">Output Canvas to use</param>
        public Polygon(List<Point> PointList, Syntax CurrSyntax, OutputCanvas OC)
        {
            this.CurrSyntax = CurrSyntax;
            this.PointList = PointList;
            this.OC = OC;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paramsArr">Parameter Array in the command</param>
        /// <param name="CurrSyntax">Syntax object of the command used to invoke the object</param>
        public Polygon(string[] paramsArr, Syntax CurrSyntax)
        {
            this.CurrSyntax = CurrSyntax;
            SetParams(paramsArr);
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paramsArr">Parameter Array in the command</param>
        /// <param name="CurrSyntax">Syntax object of the command used to invoke the object</param>
        /// <param name="OC">Output Canvas to use</param>
        public Polygon(string[] paramsArr, Syntax CurrSyntax, OutputCanvas OC)
        {
            this.CurrSyntax = CurrSyntax;
            SetParams(paramsArr);
            this.OC = OC;
        }
        /// <summary>
        /// Parameter setter for the object
        /// </summary>
        /// <param name="paramsArr">Parameter Array in the command</param>
        public override void SetParams(string[] paramsArr)
        {
            ParamType = Array.FindIndex<Syntax>(SyntaxArr, row => row.SyntaxString == CurrSyntax.SyntaxString);
            //Check for multipleswitch cases at once
            switch (ParamType)
            {
                case var s when new[] { 0 }.Contains(s):
                    try
                    {
                        foreach(string param in paramsArr)
                        {
                            string tmp = param.Trim();
                            tmp = tmp.Trim(new char[] { '(', ')' });
                            string[] split = Helper.SplitStringBrackets(tmp, ",");
                            int X = OC.Evaluator.Evaluate<int>(split[0]),
                                Y = OC.Evaluator.Evaluate<int>(split[1]);
                            PointList.Add(new Point(X, Y));
                        }
                    }
                    catch (Exception e)
                    {
                        throw new SyntaxException(e.Message);
                    }
                    break;

            }
        }

        public override void Run()
        {
            if (!OC.ShapeFill)
            {
                switch (ParamType)
                {
                    case var s when new[] { 0 }.Contains(s):
                        OC.g.DrawPolygon(OC.Drawer, PointList.ToArray());
                        break;
                }
            }
            else
            {
                SolidBrush reqBrush = new SolidBrush(OC.Drawer.Color);
                switch (ParamType)
                {
                    case var s when new[] { 0 }.Contains(s):
                        OC.g.FillPolygon(reqBrush, PointList.ToArray());
                        break;
                }
            }
        }
        public override void Run(OutputCanvas OC)
        {
            this.OC = OC;
            Run();
        }

        public override string CommandName()
        {
            return Name;
        }

        public override Syntax[] CommandSyntaxArr()
        {
            return SyntaxArr;
        }

        public override Syntax CommandCurrSyntax()
        {
            return CurrSyntax;
        }

        public override void CommandCurrSyntax(Syntax CurrSyntax)
        {
            this.CurrSyntax = CurrSyntax;
        }

        public override OutputCanvas CommandOC()
        {
            return OC;
        }

        public override void CommandOC(OutputCanvas OC)
        {
            this.OC = OC;
        }
    }
}
