﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// Interface for the Graphic commands
    /// </summary>
    public interface IGraphic : ICommand
    {
        /// <summary>
        /// Run the command of the current object
        /// </summary>
        /// <param name="oc">Output Canvas to use</param>
        void Run(OutputCanvas oc);

    }
}
