﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Drawing;

namespace GPLApplication
{
    /// <summary>
    /// Clear command class
    /// </summary>
    class Clear : AKeyword
    {
        /// <summary>
        /// Name of the command
        /// </summary>
        public const string Name = "clear";
        /// <summary>
        /// Object containing the string and regex of the syntax allowed for the command
        /// </summary>
        public static readonly Syntax[] SyntaxArr = new[]{
            new Syntax(Name, new Regex(@"^\b(?<commandName>" + Name + @")\b(?<commandParams>)$"))
        };
        /// <summary>
        /// The current syntax used in the command object
        /// </summary>
        public Syntax CurrSyntax { get; set; }
        /// <summary>
        /// OutputCanvas to be used for the command
        /// </summary>
        public OutputCanvas OC { set; get; }
        /// <summary>
        /// Integer value to determine which setter and process command to use for the run command
        /// </summary>
        private int ParamType { set; get; }
        /// <summary>
        /// Constructor
        /// </summary>
        public Clear()
        {
            CurrSyntax = SyntaxArr[0];
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="OC">OutputCanvas to use</param>
        public Clear(OutputCanvas OC) : this()
        {
            this.OC = OC;
        }
        public override Syntax CommandCurrSyntax()
        {
            return CurrSyntax;
        }

        public override void CommandCurrSyntax(Syntax CurrSyntax)
        {
            this.CurrSyntax = CurrSyntax;
        }

        public override string CommandName()
        {
            return Name;
        }

        public override OutputCanvas CommandOC()
        {
            return OC;
        }

        public override void CommandOC(OutputCanvas OC)
        {
            this.OC = OC;
        }

        public override Syntax[] CommandSyntaxArr()
        {
            return SyntaxArr;
        }

        public override void Run()
        {
            switch (ParamType)
            {
                case var s when new[] { 0 }.Contains(s):
                    OC.g.Clear(Color.White);
                    break;
            }
        }
        public override void Run(OutputCanvas oc)
        {
            this.OC = oc;
            Run();
        }
        public override void SetParams(string[] paramsArr)
        {
            ParamType = Array.FindIndex<Syntax>(SyntaxArr, row => row.SyntaxString == CurrSyntax.SyntaxString);
            switch (ParamType)
            {
                case var s when new[] { 0 }.Contains(s):
                    break;
            }
        }
    }
}
