﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLApplication { 
    /// <summary>
    /// Abstract class for the keyword type commands
    /// </summary>
    public abstract class AKeyword : IGraphic
    {
        public abstract Syntax CommandCurrSyntax();
        public abstract void CommandCurrSyntax(Syntax CurrSyntax);
        public abstract string CommandName();
        public abstract OutputCanvas CommandOC();
        public abstract void CommandOC(OutputCanvas OC);
        public abstract Syntax[] CommandSyntaxArr();
        public abstract void Run(OutputCanvas oc);
        public abstract void Run();
        public abstract void SetParams(string[] paramsArr);
    }
}
