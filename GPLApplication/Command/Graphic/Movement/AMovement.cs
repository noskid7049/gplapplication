﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// Abstract clas for Movement type commands
    /// </summary>
    public abstract class AMovement : IGraphic
    {
        public abstract Syntax CommandCurrSyntax();
        public abstract void CommandCurrSyntax(Syntax CurrSyntax);
        public abstract string CommandName();
        public abstract OutputCanvas CommandOC();
        public abstract void CommandOC(OutputCanvas OC);
        public abstract Syntax[] CommandSyntaxArr();
        public abstract void SetParams(string[] paramsArr);
        public abstract void Run();
        public abstract void Run(OutputCanvas oc);
    }
}
