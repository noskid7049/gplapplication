﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace GPLApplication
{
    /// <summary>
    /// Drawto Command class
    /// </summary>
    public class DrawTo : AMovement
    {
        /// <summary>
        /// Name of the command
        /// </summary>
        public const string Name = "drawto";
        /// <summary>
        /// Object containing the string and regex of the syntax allowed for the command
        /// </summary>
        public static readonly Syntax[] SyntaxArr = new[]{
            new Syntax(Name+" X,Y", new Regex(@"^\b(?<commandName>" + Name + @")\b\s{1}(?<commandParams>\w+\s*,\s*\w+)$"))
        };
        /// <summary>
        /// The current syntax used in the command object
        /// </summary>
        public Syntax CurrSyntax { get; set; }
        /// <summary>
        /// The end point for the movement command
        /// </summary>
        public Point EndPoint { set; get; }
        /// <summary>
        /// OutputCanvas to be used for the command
        /// </summary>
        public OutputCanvas OC { set; get; }
        /// <summary>
        /// Integer value to determine which setter and process command to use for the run command
        /// </summary>
        private int ParamType { set; get; }
        public DrawTo()
        {
            this.CurrSyntax = SyntaxArr[0];
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="EndPoint">Point where the command will end</param>
        public DrawTo(Point EndPoint) : this()
        {
            this.EndPoint = EndPoint;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="EndPoint">Point where the command will end</param>
        /// <param name="OC">Output Canvas to use</param>
        public DrawTo(Point EndPoint, OutputCanvas OC) : this()
        {
            this.EndPoint = EndPoint;
            this.OC = OC;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paramsArr">String of the parameters in the command</param>
        public DrawTo(string[] paramsArr) : this()
        {
            SetParams(paramsArr);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paramsArr">String of the parameters in the command</param>
        /// <param name="OC">Output Canvas to use</param>
        public DrawTo(string[] paramsArr, OutputCanvas OC) : this()
        {
            SetParams(paramsArr);
            this.OC = OC;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="EndPoint">Point where the command will end</param>
        /// <param name="CurrSyntax">Syntax object of the command used to invoke the object</param>
        public DrawTo(Point EndPoint, Syntax CurrSyntax)
        {
            this.CurrSyntax = CurrSyntax;
            this.EndPoint = EndPoint;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="EndPoint">Point where the command will end</param>
        /// <param name="CurrSyntax">Syntax object of the command used to invoke the object</param>
        /// <param name="OC">Output Canvas to use</param>
        public DrawTo(Point EndPoint, Syntax CurrSyntax, OutputCanvas OC)
        {
            this.CurrSyntax = CurrSyntax;
            this.EndPoint = EndPoint;
            this.OC = OC;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paramsArr">Parameter Array in the command</param>
        /// <param name="CurrSyntax">Syntax object of the command used to invoke the object</param>
        public DrawTo(string[] paramsArr, Syntax CurrSyntax)
        {
            this.CurrSyntax = CurrSyntax;
            SetParams(paramsArr);
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paramsArr">Parameter Array in the command</param>
        /// <param name="CurrSyntax">Syntax object of the command used to invoke the object</param>
        /// <param name="OC">Output Canvas to use</param>
        public DrawTo(string[] paramsArr, Syntax CurrSyntax, OutputCanvas OC)
        {
            this.CurrSyntax = CurrSyntax;
            SetParams(paramsArr);
            this.OC = OC;
        }
        public override void SetParams(string[] paramsArr)
        {
            // Find the required syntax from the syntax arr so that further conversion can be done based on the string params
            ParamType = Array.FindIndex<Syntax>(SyntaxArr, row => row.SyntaxString == CurrSyntax.SyntaxString);
            switch (ParamType)
            {
                case var s when new[] { 0 }.Contains(s):
                    {
                        // Parse the parameters (in string) to required type and throw Exception if error
                        // This will allow for overloading of the command in future if required
                        int X, Y;
                        try
                        {
                            X = OC.Evaluator.Evaluate<int>(paramsArr[0]);
                            Y = OC.Evaluator.Evaluate<int>(paramsArr[1]);
                        }
                        catch (Exception e)
                        {
                            throw new SyntaxException(e.Message);
                        }
                        EndPoint = new Point(X, Y);
                        break;
                    }
            }
        }
        public override void Run()
        {
            // Based on the current active param type, run the command as required
            // This will allow for overloading of the command in future if required
            switch (ParamType)
            {
                case var s when new[] { 0 }.Contains(s):
                    OC.g.DrawLine(OC.Drawer, OC.CurrentPos, EndPoint);
                    OC.CurrentPos = EndPoint;
                    break;
            }
        }
        public override void Run(OutputCanvas OC)
        {
            this.OC = OC;
            Run();
        }

        public override string CommandName()
        {
            return Name;
        }

        public override Syntax[] CommandSyntaxArr()
        {
            return SyntaxArr;
        }

        public override Syntax CommandCurrSyntax()
        {
            return CurrSyntax;
        }

        public override void CommandCurrSyntax(Syntax CurrSyntax)
        {
            this.CurrSyntax = CurrSyntax;
        }

        public override OutputCanvas CommandOC()
        {
            return OC;
        }

        public override void CommandOC(OutputCanvas OC)
        {
            this.OC = OC;
        }
    }
}
