﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodingSeb.ExpressionEvaluator;
using System.Drawing;

namespace GPLApplication
{
    /// <summary>
    /// Canvas Wrapper with graphics and Drawers and other extra info
    /// </summary>
    public class OutputCanvas
    {
        /// <summary>
        /// Graphics to use in the canvas
        /// </summary>
        public Graphics g { get; private set; }
        /// <summary>
        /// Pen to use in the canvas
        /// </summary>
        public Pen Drawer { private set; get; }
        /// <summary>
        /// Current position of the pen
        /// </summary>
        public Point CurrentPos { set; get; }
        /// <summary>
        /// Shape fill status for the following shapes
        /// </summary>
        public bool ShapeFill { set; get; }
        /// <summary>
        /// Evaluator to be used for the current OC
        /// </summary>
        public ExpressionEvaluator Evaluator { set; get; }
        /// <summary>
        /// List of methods along with its commands
        /// </summary>
        public Dictionary<string, Method> MethodList { set; get; }
        /// <summary>
        /// Constructor
        /// </summary>
        public OutputCanvas()
        {

            //Create a default standard pen for starters
            Drawer = new Pen(AppProperties.PenColor, AppProperties.PenWidth);
            // Initialize the X and Y position of the pen initially. Set to 0,0
            CurrentPos = AppProperties.InitialPos;
            // Get the shape fill property from the app properties
            ShapeFill = AppProperties.ShapeFill;
            // Create a new instance of the expression evaluator
            Evaluator = new ExpressionEvaluator();
            // Instantiate method list
            MethodList = new Dictionary<string, Method>();
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="g">Graphic to use in the canvas</param>
        public OutputCanvas(Graphics g) : this()
        {
            // Set the graphics reference to the required graphics
            this.g = g;

        }
        /*   
                Getter and Setter for Pen(Drawer) properties
         */
        /// <summary>
        /// Setter for pen color
        /// </summary>
        /// <param name="color">Color to be set for the drawer</param>
        public void SetPenColor(Color color)
        {
            Drawer.Color = color;
            //AppProperties.PenColor = color;
        }
        /// <summary>
        /// PenColor Getter
        /// </summary>
        /// <returns>Color of the pen</returns>
        public Color GetPenColor()
        {
            return Drawer.Color;
        }
        /// <summary>
        /// PenWidth setter
        /// </summary>
        /// <param name="width">Width of the pen to be set</param>
        public void SetPenWidth(float width)
        {
            Drawer.Width = width;
            //AppProperties.PenWidth = width;
        }
        /// <summary>
        /// PenWidth Getter
        /// </summary>
        /// <returns>Width of the pen</returns>
        public float GetPenWidth()
        {
            return Drawer.Width;
        }
    }
}
