﻿using CodingSeb.ExpressionEvaluator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// Static class containing the helper function to be used in the code
    /// </summary>
    public static class Helper
    {
        /// <summary>
        /// Split the given string with character excluding ones inside double quotes(");
        /// </summary>
        /// <param name="line">Line/ String to split</param>
        /// <param name="character">Character to use for splitting</param>
        /// <returns>Array of split string</returns>
        public static string[] SplitString(string line, string character)
        {
            // Split the line by the given character if the line is not null and the character is not present in between doublequotes
            string[] pieces = Regex.Split(line, character + "(?=(?:[^']*'[^']*')*[^']*$)").Where(exp => (exp) != null).ToArray();
            // Remove the single quotes in the splitted pieces
            pieces = pieces.Select(piece =>
            {
                piece = piece.Replace("'", "");
                return piece;
            }).ToArray();
            //character + "(?=(?:[^']*'[^']*')*[^']*$)"
            return pieces;
        }
        /// <summary>
        /// Split the given string with given characters excluding "("
        /// </summary>
        /// <param name="line">The line to split</param>
        /// <param name="character">The character to use to split the data</param>
        /// <returns>Returns the array of split string</returns>
        public static string[] SplitStringBrackets(string line, string character)
        {
            // Generate regex
            Regex regex = new Regex(@"\([^\)]+?[\)]|[\""].+?[\""] |[^" + character + @"]+");
            // Searhc for matches
            MatchCollection matches = regex.Matches(line);
            List<string> pieces = new List<string>();
            // Loop through the matches and add the values
            foreach(Match match in matches)
            {
                pieces.Add(match.Value);
            }
            return pieces.ToArray();
        }
        /// <summary>
        /// Function to join dictionary keys
        /// </summary>
        /// <param name="dict">Dictionary to join</param>
        /// <param name="separator">Separator to use when joining the dictionary keys</param>
        /// <returns></returns>
        public static string JoinDictionaryKeys(Dictionary<string, string> dict, string separator = "|")
        {
            return string.Format("{0}", string.Join(separator, dict.Keys));
        }
        /// <summary>
        /// Check if the curretn command is an arithmetic operation with asiignment
        /// </summary>
        /// <param name="command">Command to check for</param>
        /// <returns>True if the command is an arithmetic operation</returns>
        public static bool IsArithmeticOperation(string command)
        {
            return RegexObject.arithmeticOperation.IsMatch(command);
        }

        /// <summary>
        /// Check if the command is a conditional or loop statement
        /// </summary>
        /// <param name="command">Command to parse</param>
        /// <param name="statement">Statement to check in the command</param>
        /// <returns>True if valid condition or loop</returns>
        public static bool IsValidConditionalOrLoop(string command, string statement)
        {
            Regex regex = RegexObject.GenerateConditionalRegex(statement);
            return regex.IsMatch(command);
        }
        /// <summary>
        /// Parse the statement to get the statement, position and type of the command
        /// </summary>
        /// <param name="command">Command to parse</param>
        /// <returns>A Dictionary with the statement, position and the type of the command </returns>
        public static Dictionary<string, string> ParseLineType(string command)
        {
            Regex regex, endRegex;
            string output = "", type = "", position = "";
            Dictionary<string, string> reqOutput = new Dictionary<string, string>();
            bool found = false;
            if (!found)
            {
                // Check if conditional statement
                foreach (string statement in AppProperties.conditionalStatements)
                {
                    regex = RegexObject.GenerateStatementStart(statement);
                    endRegex = RegexObject.GenerateStatementEnd(statement);
                    // if the command matches the regex for the start of the conditional statement
                    if (regex.IsMatch(command))
                    {
                        output = statement;
                        position = "start";
                        type = "conditional";
                        found = true;
                    }
                    // if the command matches the regex for the end of the conditional statement
                    else if (endRegex.IsMatch(command))
                    {
                        output = statement;
                        position = "end";
                        type = "conditional";
                        found = true;
                    }
                }
            }
            if (!found)
            {
                // Check if the statement is a loopstatement
                foreach (string statement in AppProperties.loopStatements)
                {
                    regex = RegexObject.GenerateStatementStart(statement);
                    endRegex = RegexObject.GenerateStatementEnd(statement);
                    // if the command matches the regex for the start of the conditional statement
                    if (regex.IsMatch(command))
                    {
                        output = statement;
                        position = "start";
                        type = "loop";
                        found = true;
                    }
                    // if the command matches the regex for the end of the conditional statement
                    else if (endRegex.IsMatch(command))
                    {
                        output = statement;
                        position = "end";
                        type = "loop";
                        found = true;
                    }
                }
            }
            if (!found)
            {
                regex = RegexObject.GenerateStatementStart("method");
                endRegex = RegexObject.GenerateStatementEnd("method");
                // if the command matches the regex for the start of the method definer statement
                if (regex.IsMatch(command))
                {
                    output = "method";
                    position = "start";
                    type = "method";
                    found = true;
                }
                // if the command matches the regex for the end of the method definer statement
                else if (endRegex.IsMatch(command))
                {
                    output = "method";
                    position = "end";
                    type = "method";
                    found = true;
                }
            }
            Console.WriteLine("Statement: " + output.Trim());
            Console.WriteLine("Position:" + position.Trim());
            Console.WriteLine("Type: " + type.Trim());
            // Add the outputs of the parse to the dictionary that is being returned
            reqOutput.Add("output", output.Trim());
            reqOutput.Add("position", position.Trim());
            reqOutput.Add("type", type.Trim());

            return reqOutput;

        }


        /// <summary>
        /// Check if the passed command is a method
        /// </summary>
        /// <param name="command">Command to check</param>
        /// <returns>True if valid command</returns>
        public static bool IsValidMethodDefiner(string command)
        {
            // Regex for the method definer
            Regex regex = RegexObject.regexMethodDefiner;
            return regex.IsMatch(command);
        }
        /// <summary>
        /// Chekc if the passed command is a valid method call
        /// </summary>
        /// <param name="command">Command to check for</param>
        /// <returns>True if valid method call</returns>
        public static bool IsValidMethodCall(string command)
        {
            Regex regex = RegexObject.regexMethodCall;
            return regex.IsMatch(command);
        }
        /// <summary>
        /// Parse the statement to get condition and statement(if inline)
        /// </summary>
        /// <param name="command">Command to parse</param>
        /// <param name="statement">The statement to check for in the command</param>
        /// <returns>A string arr with condition in 0 index and command in 1 index</returns>
        public static Dictionary<string, string> ParseConditionalOrLoop(string command, string statement)
        {
            // Get the regex for the required parsing
            Regex regex = RegexObject.GenerateConditionalRegex(statement);

            return GetGroupsFromRegex(command, regex);
        }
        /// <summary>
        /// Function to parse a method definer
        /// </summary>
        /// <param name="command">Command to parse</param>
        /// <returns>Parsed data with method name and param string</returns>
        public static Dictionary<string, string> ParseMethodDefiner(string command)
        {
            // Regex for the method definer
            Regex regex = RegexObject.regexMethodDefiner;

            return GetGroupsFromRegex(command, regex);

        }
        /// <summary>
        /// Helper function to parse a method call statement
        /// </summary>
        /// <param name="command">Command to parse</param>
        /// <returns>Dictionary with the parsed data from the method call</returns>
        public static Dictionary<string, string> ParseMethodCall(string command)
        {
            // Regex for the method definer
            Regex regex = RegexObject.regexMethodCall;

            return GetGroupsFromRegex(command, regex);

        }
        /// <summary>
        /// Helper function to get the groups from a regex match
        /// </summary>
        /// <param name="command">Command to get the groups from</param>
        /// <param name="regex">Regex to use</param>
        /// <returns>Parsed data i.e. required groups from the regex</returns>
        public static Dictionary<string, string> GetGroupsFromRegex(string command, Regex regex)
        {
            Dictionary<string, string> output = new Dictionary<string, string>();
            // Search for matches with group condition and command from the regex
            MatchCollection matches = regex.Matches(command);

            foreach (Match m in matches)
            {
                GroupCollection groups = m.Groups;
                foreach (Group g in groups)
                {
                    output.Add(g.Name, g.Value);
                }
            }
            return output;
        }

    }
    /// <summary>
    /// Struct for the errors in the Command Box
    /// </summary>
    public struct CError
    {
        /// <summary>
        /// Index of the error text
        /// </summary>
        public int Index { set; get; }
        /// <summary>
        /// Length of the error text
        /// </summary>
        public int Length { set; get; }
        /// <summary>
        /// The error text
        /// </summary>
        public string ErrTxt { set; get; }
        /// <summary>
        /// Possible fix for the error
        /// </summary>
        public string PossibleFix { set; get; }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="index">Index of the error txt</param>
        /// <param name="length">Length of the error txt</param>
        /// <param name="errTxt">Error txt</param>
        /// <param name="possibleFix">Possible fix for the error</param>
        public CError(int index, int length, string errTxt, string possibleFix)
        {
            Index = index;
            Length = length;
            ErrTxt = errTxt;
            PossibleFix = possibleFix;
        }
    }
    /// <summary>
    /// Struct for the success in the Command Box
    /// </summary>
    public struct CSuccess
    {
        /// <summary>
        /// Index for the success text
        /// </summary>
        public int Index { set; get; }
        /// <summary>
        /// Length of the success text
        /// </summary>
        public int Length { set; get; }
        /// <summary>
        /// Text of the success line
        /// </summary>
        public string LineString { set; get; }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="index">Index of the success txt</param>
        /// <param name="length">Length of the success txt</param>
        /// <param name="line">Line text</param>
        public CSuccess(int index, int length, string line)
        {
            Index = index;
            Length = length;
            LineString = line;
        }
    }
    /// <summary>
    /// Struct to store the output of the parsed syntax from the Command box
    /// </summary>
    public struct SyntaxParseOutput
    {
        /// <summary>
        /// List of error
        /// </summary>
        public List<CError> ErrorList { set; get; }
        /// <summary>
        /// List of success
        /// </summary>
        public List<CSuccess> SuccessList { set; get; }
        /// <summary>
        /// List of variables
        /// </summary>
        public List<string> Variables { set; get; }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="errorList">List of error</param>
        /// <param name="successList">List of success</param>
        /// <param name="variables">Varialbes list</param>
        public SyntaxParseOutput(List<CError> errorList, List<CSuccess> successList, List<string> variables)
        {
            ErrorList = errorList;
            SuccessList = successList;
            Variables = variables;
        }
    }
    /// <summary>
    /// Struct for the active statement
    /// </summary>
    public struct ActiveStatement
    {
        /// <summary>
        /// The statement
        /// </summary>
        public string Statement { set; get; }
        /// <summary>
        /// Bool to check if the statement should run
        /// </summary>
        public bool ShouldRun { set; get; }
        /// <summary>
        /// Type of the statement
        /// </summary>
        public string Type { set; get; }
        /// <summary>
        /// Condition of the statement if available
        /// </summary>
        public string Condition { set; get; }
        /// <summary>
        /// List of commands
        /// </summary>
        public List<string> CommandList { set; get; }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="statement">Statement of the command</param>
        /// <param name="shouldRun">Bool to check if the statement should run</param>
        /// <param name="type">Type of the statement</param>
        /// <param name="condition">Condition of the statement if present</param>
        /// <param name="commandList">List of commands</param>
        public ActiveStatement(string statement, bool shouldRun, string type, string condition, List<string> commandList)
        {
            Statement = statement;
            ShouldRun = shouldRun;
            Type = type;
            Condition = condition;
            CommandList = commandList;
        }
    }
}
