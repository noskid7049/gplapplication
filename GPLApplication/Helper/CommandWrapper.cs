﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace GPLApplication
{
    /// <summary>
    /// Wrapper Class for the commands
    /// </summary>
    public class CommandWrapper
    {
        /// <summary>
        /// Name of the command
        /// </summary>
        public string CommandName { get; private set; }
        /// <summary>
        /// Param string
        /// </summary>
        public string CommandParams { get; private set; }
        /// <summary>
        /// Array of string of the params
        /// </summary>
        public string[] CommandParamsArr { get; private set; }
        /// <summary>
        /// Reference to the actual command
        /// </summary>
        public ICommand ActualCommand { set; get; }
        /// <summary>
        /// ArrayList of errors when parsing the command
        /// </summary>
        public ArrayList ErrorList { set; get; }
        /// <summary>
        /// LineNumber of the command in case of multiline commands
        /// </summary>
        public int LineNumber { set; get; }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="CommandName">Name of the command</param>
        /// <param name="CommandParams">Params used</param>
        public CommandWrapper(string CommandName, string CommandParams)
        {
            SetParams(CommandName, CommandParams);
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="CommandName">Name of the command</param>
        /// <param name="CommandParams">Params used</param>
        /// <param name="ActualCommand">Reference to the actual command object</param>
        public CommandWrapper (string CommandName, string CommandParams, ICommand ActualCommand)
        {
            SetParams(CommandName, CommandParams);
            SetActualCommand(ActualCommand);
        }
        /// <summary>
        /// Parans setter 
        /// </summary>
        /// <param name="CommandName">Name of the command</param>
        /// <param name="CommandParams">Params used</param>
        public void SetParams(string CommandName, string CommandParams)
        {
            ErrorList = new ArrayList();
            LineNumber = 0;
            this.CommandName = CommandName;
            this.CommandParams = CommandParams;
            this.CommandParamsArr = Helper.SplitStringBrackets(CommandParams, ",");
        }
        /// <summary>
        /// ActualCommand setter
        /// </summary>
        /// <param name="ActualCommand">Reference to the actual command object</param>
        public void SetActualCommand(ICommand ActualCommand)
        {
            this.ActualCommand = ActualCommand;
        }
    }
}
