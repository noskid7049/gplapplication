﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// Wrapper class for the syntaxw
    /// </summary>
    public class Syntax
    {
        /// <summary>
        /// String of the syntax to be used for display/comparison
        /// </summary>
        public string SyntaxString { get; set; }
        /// <summary>
        /// Regex of the syntax
        /// </summary>
        public Regex SyntaxRegex { get; set; }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="syntaxString">String of the syntax</param>
        /// <param name="syntaxRegex">Regex of the syntax</param>
        public Syntax(string syntaxString, Regex syntaxRegex)
        {
            this.SyntaxString = syntaxString;
            this.SyntaxRegex = syntaxRegex;
        }

    }
}
