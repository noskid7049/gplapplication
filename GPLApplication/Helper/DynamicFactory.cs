﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// Dynamic factory to create class reference using string
    /// </summary>
    /// <typeparam name="T">Type of classes to create</typeparam>
    public class DynamicFactory<T>
    {
        /// <summary>
        /// Function to create the object of the required class
        /// </summary>
        /// <param name="className">Classname of the object to create</param>
        /// <returns>Object of the required class</returns>
        public static T Create(string className)
        {
            Type t = typeof(T);
            return (T)Activator.CreateInstance(
                       t.Assembly.FullName,
                       t.Namespace + "." + className
                     ).Unwrap();
        }
    }
}
