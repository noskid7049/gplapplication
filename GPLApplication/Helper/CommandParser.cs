﻿using CodingSeb.ExpressionEvaluator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GPLApplication
{
    /// <summary>
    /// Static class for parsing/ helping to parse/ run the commands
    /// </summary>
    public static class CommandParser
    {
        /// <summary>
        /// Function to run the multiline commands
        /// </summary>
        /// <param name="commands">List of commands to run</param>
        /// <param name="outputCanvas">Canvas to run the commands on</param>
        /// <param name="runCommand">Bool to check if the command should run</param>
        public static void RunMultiLineCommands(List<string> commands, OutputCanvas outputCanvas,bool runCommand = true)
        {
            Stack<ActiveStatement> activeStatementStack = new Stack<ActiveStatement>();
            ActiveStatement activeStatement = new ActiveStatement();
            activeStatement.Statement = "root";
            activeStatement.ShouldRun = true;
            activeStatement.Type = "root";
            activeStatement.Condition = "true";
            activeStatement.CommandList = new List<string>();

            activeStatementStack.Push(activeStatement);

            for (int i = 0; i < commands.Count; i++)
            {
                string currCommand = commands[i].Trim();
                currCommand = currCommand.ToLower();
                if (currCommand == "") continue;
                string statement, type, position;
                Dictionary<string, string> currCommandParse = Helper.ParseLineType(currCommand);
                // Check if the current command is a conditional, loop or method definer
                currCommandParse.TryGetValue("output", out statement);
                // Get the type and position of the command
                currCommandParse.TryGetValue("type", out type);
                currCommandParse.TryGetValue("position", out position);
                // If some statement is detected
                if (statement != "")
                {
                    // Check if the command is of correct syntax and the position is start
                    if (position == "start")
                    {
                        // Prepare the active statement for further processing
                        activeStatement = new ActiveStatement();
                        activeStatement.CommandList = new List<string>();
                        activeStatement.Statement = statement;
                        activeStatement.Type = type;
                        // Check if the command is conditional
                        if (Helper.IsValidConditionalOrLoop(currCommand, statement) && (type == "conditional" || type == "loop"))
                        {
                            // Parse the statement to get required outputs
                            Dictionary<string, string> outputs = Helper.ParseConditionalOrLoop(currCommand, statement);
                            string condition, command;
                            try
                            {
                                // Get the command and condition from the parse
                                outputs.TryGetValue("condition", out condition);
                                outputs.TryGetValue("command", out command);
                            }
                            catch (Exception e)
                            {
                                throw new SyntaxException($"Error parsing statement\n{e.Message}");
                            }
                            Console.WriteLine("Condition:" + condition);
                            Console.WriteLine("Command:" + command);
                            Console.WriteLine("Statement:" + statement);

                            activeStatement.Condition = condition;
                            if (activeStatement.Statement == "loop for")
                            {
                                outputCanvas.Evaluator.Variables["loopforstack" + activeStatementStack.Count] = int.Parse(condition);
                                activeStatement.Condition = "loopforstack" + activeStatementStack.Count + ">0";
                                condition = activeStatement.Condition;
                                //currCommand = $"loop for({condition})";
                            }
                            // If the condition evaluates to true and parent is true
                            if ((bool)outputCanvas.Evaluator.Evaluate(condition) && activeStatementStack.Peek().ShouldRun)
                            {
                                activeStatement.ShouldRun = true;
                            }
                            else
                            {
                                activeStatement.ShouldRun = false;
                            }
                            // If inline conditional
                            if (type == "conditional" && command != "")
                            {
                                foreach (KeyValuePair<string, object> entry in outputCanvas.Evaluator.Variables)
                                {
                                    Console.WriteLine($"{entry.Key}: {entry.Value}");
                                }
                                Console.WriteLine("Inline Condition runs : " + activeStatement.ShouldRun);
                                // Pop the run commands stack and run if true
                                if (activeStatement.ShouldRun)
                                {
                                    RunCommand(command, outputCanvas, runCommand, (i + 1));
                                }
                            }
                            else
                            {
                                // Push the current active statement to the stack
                                activeStatementStack.Push(activeStatement);
                            }
                        }
                        // Check if the current method is a method definer
                        else if (Helper.IsValidMethodDefiner(currCommand) && type == "method")
                        {
                            // Parse the statement to get required outputs
                            Dictionary<string, string> outputs = Helper.ParseMethodDefiner(currCommand);
                            string methodName, paramString;
                            try
                            {
                                // Get the command and condition from the parse
                                outputs.TryGetValue("methodName", out methodName);
                                outputs.TryGetValue("paramString", out paramString);
                            }
                            catch (Exception e)
                            {
                                throw new SyntaxException($"Error parsing statement\n{e.Message}");
                            }
                            // Set the statement name to the current method name parsed
                            activeStatement.Statement = methodName;
                            // Always evaluate the condition of the method to true
                            activeStatement.Condition = "true";
                            // Always evaluate the condition to false for method
                            activeStatement.ShouldRun = false;
                            // Create a Method object using the above data
                            string[] paramList = Helper.SplitString(paramString, ",");

                            Method tmp = new Method(methodName, paramList, new List<string>(), outputCanvas);
                            outputCanvas.MethodList[methodName] = tmp;
                            // Push the current active statement to the stack
                            activeStatementStack.Push(activeStatement);

                        }
                    }
                    else if (position == "end")
                    {
                        // Get the active statement from the stack
                        ActiveStatement currActiveStatement = activeStatementStack.Peek();
                        string condition = currActiveStatement.Condition;

                        if (currActiveStatement.Statement == "loop for")
                        {
                            currActiveStatement.CommandList.Add("loopForStack" + (activeStatementStack.Count - 1) + "--");
                        }
                        // Throw error if the end statement is not as expected
                        if (currActiveStatement.Statement != statement && currActiveStatement.Type != statement)
                        {
                            throw new SyntaxException($"Expected: end {currActiveStatement.Statement}  Got: end {statement} \nLine Number: {i + 1}");
                        }
                        // If the end is of loop and the condition still evaluates to true
                        while (currActiveStatement.Type == "loop" && (bool)outputCanvas.Evaluator.Evaluate(condition))
                        {
                            RunMultiLineCommands(currActiveStatement.CommandList, outputCanvas,runCommand);
                        }
                        // Remove the current statement from the stack
                        activeStatementStack.Pop();


                    }
                }
                else
                {
                    // Get the active statement from the stack
                    ActiveStatement actStatement = activeStatementStack.Peek();
                    // Check if the active statement is method and if true, add the current command to the statement command list
                    if (actStatement.Type == "method")
                    {
                        outputCanvas.MethodList[actStatement.Statement].CommandList.Add(currCommand);
                    }
                    // Run the statement if the parent condition is true
                    if (actStatement.ShouldRun)
                    {
                        try
                        {
                            RunCommand(currCommand, outputCanvas,runCommand, (i + 1));
                        }
                        catch (Exception e)
                        {
                            throw new Exception($"{e.Message} \nLine: {i + 1}");
                        }
                    }

                }
                int start = 0;
                string statementType = activeStatementStack.Peek().Type;
                if (statement != "" && position == "start") start = 1;
                // Add to all parent if not method
                // 
                if (statementType != "method")
                {
                    // Append the current command to all the active statement stack
                    for (int index = start; index < (activeStatementStack.Count - 1); index++)
                    {
                        activeStatementStack.Skip(index).First().CommandList.Add(currCommand);
                    }
                }
            }
            if(activeStatementStack.Count > 1)
            {
                throw new SyntaxException("Missing end statement for "+ activeStatementStack.Peek().Statement + " " + activeStatementStack.Peek().Type);
            }
        }
        /// <summary>
        /// Run individual command
        /// </summary>
        /// <param name="command">Command to run</param>
        /// <param name="outputCanvas">Canvas to run the command on</param>
        /// <param name="lineNum">Line Number of the current command</param>
        /// <param name="runCommand">Bool to check if the command should run</param>
        public static void RunCommand(string command, OutputCanvas outputCanvas, bool runCommand, int lineNum = 0)
        {
            command = command.Trim();
            if (Helper.IsArithmeticOperation(command))
            {
                try
                {
                    outputCanvas.Evaluator.Evaluate(command);
                }
                catch (Exception err)
                {
                    throw new SyntaxException($"{err.Message}");
                }
            }
            else if (Helper.IsValidMethodCall(command))
            {
                // Parse the statement to get required outputs
                Dictionary<string, string> outputs = Helper.ParseMethodCall(command);
                string methodName, paramString;
                try
                {
                    // Get the command and condition from the parse
                    outputs.TryGetValue("methodName", out methodName);
                    outputs.TryGetValue("paramString", out paramString);
                }
                catch (Exception e)
                {
                    throw new SyntaxException($"Error parsing statement\n{e.Message}");
                }
                if (outputCanvas.MethodList.ContainsKey(methodName))
                {
                    Method requiredMethod = outputCanvas.MethodList[methodName];
                    requiredMethod.Run(paramString);
                }
            }
            else
            {
                //foreach (KeyValuePair<string, object> entry in outputCanvas.Evaluator.Variables)
                //{
                //    Console.WriteLine($"{entry.Key}: {entry.Value}");
                //}
                CommandWrapper commandWrapper = CommandFactory.getCommand(command, outputCanvas);
                commandWrapper.LineNumber = lineNum;
                ICommand actualCommand = commandWrapper.ActualCommand;
                if (actualCommand != null && commandWrapper.ErrorList.Count == 0 && runCommand)
                {
                    actualCommand.Run();
                }
                else if (commandWrapper.ErrorList.Count > 0)
                {
                    foreach (string error in commandWrapper.ErrorList)
                    {
                        throw new SyntaxException($"{error}");
                    }
                }
            }
        }
        /// <summary>
        /// Parse input command
        /// </summary>
        /// <param name="line">Line to parse</param>
        /// <returns>CommandWrapper object with name and params</returns>
        public static CommandWrapper ParseLine(string line)
        {

            //Clean the line as required
            line = line.ToLower().Trim();
            //Split the line by ' '(a single space)
            string[] split = Helper.SplitString(line, " ");
            //Get the required command name
            string commandName = split[0],
            argumentsString = "";
            if (split.Length > 1)
            {
                argumentsString = split[1];
            }

            Console.WriteLine(argumentsString);
            return new CommandWrapper(commandName, argumentsString);
        }
        /// <summary>
        /// Command Box text parser
        /// </summary>
        /// <param name="box">RichTextBox to parse the data</param>
        /// <param name="keys">Command Keys to use to allow in the parsing</param>
        /// <returns></returns>
        public static SyntaxParseOutput ParseCommandBox(RichTextBox box, Dictionary<string, string>.KeyCollection keys)
        {
            List<CSuccess> successList = new List<CSuccess>();
            List<CError> errorList = new List<CError>();
            List<string> variables = new List<string>();
            // Convert all the text in the TextBox to lower
            string boxText = box.Text.Trim();
            boxText = boxText.ToLower();
            string[] lines = Helper.SplitString(boxText, "\n");
            //Initially set that all the lines has no commands
            List<bool> commandFoundInLine = new List<bool>(lines.Length);
            for (int i = 0; i < lines.Length; i++) commandFoundInLine.Add(false);

            bool currentCommandMatched = false;
            ICommand reqCommand;
            foreach (string key in keys)
            {
                string regex = @"\b(" + key + @")\b.*$";

                // Check if any portion of the text matched the start with any of our availble command
                MatchCollection matches = Regex.Matches(boxText, regex, RegexOptions.Multiline);

                foreach (Match m1 in matches)
                {
                    // Get the text in the matched line
                    string currLine = boxText.Substring(m1.Index, m1.Length);
                    int currLineNumber = box.GetLineFromCharIndex(m1.Index);

                    commandFoundInLine[currLineNumber] = true;
                    // If matches, create a command obj of the command to get all the syntax
                    reqCommand = CommandFactory.Create(AppProperties.commandNameToClassName[key]);
                    Syntax[] synArr = reqCommand.CommandSyntaxArr();

                    currentCommandMatched = false;
                    // Get the text of the current line
                    currLine = boxText.Substring(m1.Index, m1.Length);
                    // For combining all the syntax
                    // Later used if syntax error occurs
                    string allSyntax = "";
                    foreach (Syntax syn in synArr)
                    {
                        allSyntax += $"{syn.SyntaxString}\n";
                        // Line matches syntax
                        if (syn.SyntaxRegex.IsMatch(currLine))
                        {
                            successList.Add(new CSuccess(m1.Index, key.Length, currLine));
                            currentCommandMatched = true;
                            break;
                        }
                    }
                    // if the current line does not match any syntax
                    if (!currentCommandMatched)
                    {
                        errorList.Add(new CError(m1.Index, key.Length, "Syntax Error Line: " + currLineNumber, $"List of Syntax: \n{allSyntax}"));
                    }
                }
            }
            /*
             * Check if any of the lines is an assignment command
             */
            // Create a temporary evaluator
            ExpressionEvaluator evaluator = new ExpressionEvaluator();
            for (int i = 0; i < commandFoundInLine.Count; i++)
            {
                string currLine = lines[i].Trim();
                if (currLine == "")
                {
                    commandFoundInLine[i] = true;
                    continue;
                };
                Console.WriteLine(currLine);
                // Get the conditional/ loop / end statement if present from the current line
                Dictionary<string, string> conditionalOrLoop = Helper.ParseLineType(currLine);
                // Continue if the current line is not an arithmetic operation
                if (Helper.IsArithmeticOperation(currLine))
                {
                    Console.WriteLine("+++++++++++++++++++++++++++++++++++++");
                    try
                    {
                        evaluator.Evaluate(currLine);
                        successList.Add(new CSuccess(box.GetFirstCharIndexFromLine(i), lines[i].Length, currLine));
                        commandFoundInLine[i] = true;
                        Console.WriteLine("Total Lines : " + commandFoundInLine.Count);
                        Console.WriteLine("Line: " + i + " " + currLine + " " + commandFoundInLine[i]);
                    }
                    catch (Exception e)
                    {
                        errorList.Add(new CError(box.GetFirstCharIndexFromLine(i), lines[i].Length, $"{e.Message} in line {i}", "Check operator syntax"));
                    }
                }
                else if (Helper.IsValidMethodCall(currLine))
                {
                    Console.WriteLine("=======================================");
                    // Parse the statement to get required outputs
                    Dictionary<string, string> outputs = Helper.ParseMethodCall(currLine);
                    string methodName, paramString;
                    try
                    {
                        // Get the command and condition from the parse
                        outputs.TryGetValue("methodName", out methodName);
                        outputs.TryGetValue("paramString", out paramString);
                        Console.WriteLine($"{methodName} :  {paramString}");
                        // Add the current method call to success msg
                        successList.Add(new CSuccess(box.GetFirstCharIndexFromLine(i), methodName.Length, currLine));
                    }
                    catch (Exception e)
                    {
                        errorList.Add(new CError(box.GetFirstCharIndexFromLine(i), currLine.Length, $"{e.Message} in line {i}", "Error parsing method call"));
                    }
                    commandFoundInLine[i] = true;
                }
                // If the line has conditional/loop/end statement
                else if (conditionalOrLoop.TryGetValue("output", out string statement))
                {
                    Console.WriteLine("----------------------------------");
                    // Get the type of the statement
                    conditionalOrLoop.TryGetValue("type", out string type);
                    conditionalOrLoop.TryGetValue("position", out string position);
                    // Check if the command is of valid conditional or loop statement
                    if (position == "start")
                    {
                        if (Helper.IsValidConditionalOrLoop(currLine, statement))
                        {
                            // Parse the statement to get required outputs
                            Dictionary<string, string> outputs = Helper.ParseConditionalOrLoop(currLine, statement);
                            string condition = "", command = "";
                            try
                            {
                                // Get the command and condition from the parse
                                outputs.TryGetValue("condition", out condition);
                                outputs.TryGetValue("command", out command);
                                Console.WriteLine(condition);
                                Console.WriteLine(command);
                            }
                            catch (Exception e)
                            {
                                throw new SyntaxException($"Error parsing statement\n{e.Message}");
                            }
                            // try to evaluate the condition and command if required
                            try
                            {
                                evaluator.Evaluate(condition);
                                if (Helper.IsArithmeticOperation(command)) evaluator.Evaluate(command);
                                successList.Add(new CSuccess(box.GetFirstCharIndexFromLine(i), statement.Length, currLine));
                            }
                            // Catch error if the evaluator throws errors
                            catch (Exception e)
                            {
                                errorList.Add(new CError(box.GetFirstCharIndexFromLine(i), lines[i].Length, $"{e.Message} in line {i}", "Check operator syntax"));
                            }
                            commandFoundInLine[i] = true;
                        }
                        else if (Helper.IsValidMethodDefiner(currLine))
                        {
                            // Parse the statement to get required outputs
                            Dictionary<string, string> outputs = Helper.ParseMethodDefiner(currLine);
                            string methodName = "", paramString = "";
                            try
                            {
                                // Get the command and condition from the parse
                                outputs.TryGetValue("methodName", out methodName);
                                outputs.TryGetValue("paramString", out paramString);

                                string[] split = Helper.SplitString(paramString, ",");
                                foreach (string param in split)
                                {
                                    evaluator.Variables[param] = 0;
                                }
                                // 7 from "method\s" i.e. skip "method" string for colouring
                                successList.Add(new CSuccess(box.GetFirstCharIndexFromLine(i) + 7, methodName.Length, currLine));
                            }
                            catch (Exception e)
                            {
                                errorList.Add(new CError(box.GetFirstCharIndexFromLine(i), currLine.Length, $"{e.Message} in line {i}", "Error parsing statement"));
                            }

                            commandFoundInLine[i] = true;
                        }
                    }
                    // Else check if the statement is of type end for the statement
                    else if (position == "end")
                    {
                        successList.Add(new CSuccess(box.GetFirstCharIndexFromLine(i), lines[i].Length, currLine));
                        commandFoundInLine[i] = true;
                    }
                }
            }
            // Get the variables list from the evaluator
            variables = new List<string>(evaluator.Variables.Keys);



            // Check if the line had any matches
            // If not, create a command error where command not found
            for (int i = 0; i < commandFoundInLine.Count; i++)
            {
                if (i < commandFoundInLine.Count && !commandFoundInLine[i])
                {
                    errorList.Add(new CError(box.GetFirstCharIndexFromLine(i), lines[i].Length, "Command Not Found", "Check available commands"));
                }
            }

            //Reorder the list by index
            List<CSuccess> tmp = successList.OrderBy(x => x.Index).ToList();
            successList = tmp;
            List<CError> tmp2 = errorList.OrderBy(x => x.Index).ToList();
            errorList = tmp2;

            return new SyntaxParseOutput(errorList, successList, variables);
        }

    }
}
