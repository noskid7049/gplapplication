﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPLApplication
{
    /// <summary>
    /// CommandFactory to genrate commands
    /// Inherits: DynamicFactory/<ICommand/>
    /// </summary>
    public class CommandFactory : DynamicFactory<ICommand>
    {
        /// <summary>
        /// Convert command input to a CommandWrapper with approriate data
        /// </summary>
        /// <param name="line">Command input</param>
        /// <param name="oc">OutputCanvas to use (null default)</param>
        /// <returns>CommandWrapper obj with name and params from the command</returns>
        public static CommandWrapper getCommand(string line, OutputCanvas oc = null)
        {
            // Parse the input line
            CommandWrapper reqCommand = CommandParser.ParseLine(line);
            // Set the command and syntax used to be null initially
            ICommand outCommand = null;
            Syntax usedSyntax = null;
            string commandName = reqCommand.CommandName.ToLower();
            string reqClassName;
            // Check if the command name has equivalent class
            if (AppProperties.commandNameToClassName.TryGetValue(commandName, out reqClassName))
            {
                try
                {
                    // Create the class object using the DynamicFactory.Create
                    outCommand = Create(reqClassName);
                    Dictionary<string, string> tmp = Helper.GetGroupsFromRegex(line, outCommand.CommandCurrSyntax().SyntaxRegex);

                    reqCommand.SetParams(reqClassName, tmp["commandParams"]);
                    // Set the OutputCanvas for the current command
                    outCommand.CommandOC(oc);
                    // Set the params for the class object
                    outCommand.SetParams(reqCommand.CommandParamsArr);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    reqCommand.ErrorList.Add(e.Message);
                }
            }
            Console.WriteLine("Errors " + reqCommand.ErrorList.Count);
            // If no error and command is found
            if (outCommand != null && reqCommand.ErrorList.Count == 0)
            {
                Console.WriteLine(outCommand.CommandName());
                reqCommand.ActualCommand = outCommand;
                // Find the syntax used in the current command
                usedSyntax = Array.Find(outCommand.CommandSyntaxArr(), row => row.SyntaxRegex.IsMatch(line));
                // Set the syntax used if found
                if (usedSyntax != null)
                {
                    reqCommand.ActualCommand.CommandCurrSyntax(usedSyntax);
                }
                else
                {
                    /* TODO (maybe): Nearest syntax match*/
                    // Very low chance code comes here as syntax are matched already when user is typing the code
                    reqCommand.ErrorList.Add("Syntax error");
                }
            }
            else
            {
                /* TODO (maybe): Nearest command match*/
                // Very low chance code comes here as commands are matched already when user is typing the code
                reqCommand.ErrorList.Add("Command not found");
            }

            return reqCommand;

        }
    }
}
